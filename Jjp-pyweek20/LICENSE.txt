Licensing
=========

The source code and level data of Fragmented ~A Dying Land~ is under the
Modified BSD License, also known as the 3-clause BSD License, which is
reproduced below:

Copyright (c) 2015, Jjp137
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

---

background.png is a modified version of the artwork featured at this link:
http://opengameart.org/content/tile
It is created by AntumDeluge, and it is licensed under the CC-BY 3.0 license,
which can be found here: https://creativecommons.org/licenses/by/3.0/

floor.png and wall.png are taken from the artwork featured at this link:
http://opengameart.org/content/extension-for-sci-fi-platformer-tiles-32x32
It is created by bart and rubberduck, and it is licensed under the CC0 license,
which can be found here: https://creativecommons.org/publicdomain/zero/1.0/

player_drone.png, player_dead.png, and enemy_drone.png are modified versions
of the artwork featured at this link:
http://opengameart.org/content/side-blaster-gfx-m484-games
It is created by Master484 (http://m484games.ucoz.com/) and it is licensed
under the CC0 license, which can be found here:
https://creativecommons.org/publicdomain/zero/1.0/

The graphics for node.png are taken from the artwork featured at this link:
http://opengameart.org/content/shmup-enemies
It is created by Buch (http://opengameart.org/users/buch), and it is licensed
under the CC0 license, which can be found here:
https://creativecommons.org/publicdomain/zero/1.0/

firewall.png is a modified version of the artwork featured at this link:
http://opengameart.org/content/seamless-pattern-glowing-coal
It is created by n4pgamer, and it is licensed under the CC0 license, which can
be found here: https://creativecommons.org/publicdomain/zero/1.0/

entry_port.png is a modified version of the artwork featured at this link:
http://opengameart.org/content/portholes
It is created by Downdate, and it is licensed under the CC-BY 3.0 license,
which can be found here: https://creativecommons.org/licenses/by/3.0/

The key graphics in pickups.png and the entirety of lock_tiles.png are
modified versions of the corresponding artwork featured at this link:
http://opengameart.org/content/dawnblocker-ortho
It is created by Buch (http://opengameart.org/users/buch), and it is licensed
under the CC0 license, which can be found here:
https://creativecommons.org/publicdomain/zero/1.0/

blue_square.png, gray_square.png, green_square.png, purple_square.png,
red_square.png, and yellow_square.png are taken from the artwork featured at
this link: http://opengameart.org/content/puzzle-game-art
It is created by Kenney (www.kenney.nl) and it is licensed under the CC0 license,
which can be found here: https://creativecommons.org/publicdomain/zero/1.0/

The computer chip graphic in pickups.png is modified from the artwork
featured at this link:
141 Military Icons Set by AngryMeteor.com under CC-BY 3.0 license -
http://opengameart.org/content/140-military-icons-set-fixed
AngryMeteor.com is the original author, and XCVG submitted this version of the
artwork. The CC-BY 3.0 license can be found here:
https://creativecommons.org/licenses/by/3.0/

The main font used in this game comes from this URL:
http://openfontlibrary.org/en/font/gidole-regular
It is created by larsenwork (https://github.com/larsenwork/Gidole)
and it is licensed under version 1.1 of the SIL Open Font License (OFL).
Information about this license, as well as the license itself, can be found
here: http://scripts.sil.org/OFL

The font used for title.png, which is used in the main menu, comes from this
URL: http://openfontlibrary.org/en/font/alegreya
It is created by Juan Pablo del Peral and it is licensed under version 1.1 of the
SIL Open Font License (OFL). Information about this license, as well as the
license itself, can be found here: http://scripts.sil.org/OFL

"Junkyard Tribe" Kevin MacLeod (incompetech.com)
Licensed under Creative Commons: By Attribution 3.0
http://creativecommons.org/licenses/by/3.0/
The song has been converted to .ogg from the original .mp3 format.

"Echoes of Time" Kevin MacLeod (incompetech.com)
Licensed under Creative Commons: By Attribution 3.0
http://creativecommons.org/licenses/by/3.0/
The song has been converted to .ogg from the original .mp3 format.

button-press.ogg, escape-key.ogg, frag-grab.ogg, frag-drop.ogg, and pickup.ogg
are modified versions of the sounds featured at this link:
http://opengameart.org/content/ui-sound-effects-library
They are created by Little Robot Sound Factory (www.littlerobotsoundfactory.com),
and they are licensed under the CC-BY 3.0 license,
which can be found here: https://creativecommons.org/licenses/by/3.0/

puzzle-finished.ogg is a modified version of the sound featured at this link:
https://www.freesound.org/people/copyc4t/sounds/141695/
It is created by copyc4t, and it is licensed under the CC-BY 3.0 license,
which can be found here: https://creativecommons.org/licenses/by/3.0/

walls-lower.ogg is a modified version of the sound featured at this link:
https://www.freesound.org/people/Benboncan/sounds/77074/
It is created by Benboncan, and it is licensed under the CC-BY 3.0 license,
which can be found here: https://creativecommons.org/licenses/by/3.0/

entry-port.ogg and respawn.ogg are modified versions of the sound featured at
this link: https://www.freesound.org/people/Benboncan/sounds/77074/
It is created by Sergenious, and it is licensed under the CC-BY 3.0 license,
which can be found here: https://creativecommons.org/licenses/by/3.0/

fire.ogg is a modified version of the sound featured at this link:
https://www.freesound.org/people/Julien%20Matthey/sounds/105016/
It is created by Julien Matthey, and it is licensed under the CC0 license,
which can be found here: https://creativecommons.org/publicdomain/zero/1.0/

enemy-attack.ogg is a modified version of the sound featured at this link:
https://www.freesound.org/people/fons/sounds/62363/
It is created by fons, and it is licensed under the CC0 license, which can be
found here: https://creativecommons.org/publicdomain/zero/1.0/

node-activate.ogg is a modified version of the sound featured at this link:
https://www.freesound.org/people/sheepfilms/sounds/153583/
It is created by sheepfilms, and it is licensed under the CC0 license, which
can be found here: https://creativecommons.org/publicdomain/zero/1.0/

All artwork, music, sounds, and fonts used in this game shall retain their
original licenses and can be used in other works, provided that the terms of
the licenses are met.

In addition, grid.png was created by me, and I hereby license it under the
CC0 license, which can be found here:
https://creativecommons.org/publicdomain/zero/1.0/

button-hover.ogg was created using the bfxr sound generator, which can be
found here: http://www.bfxr.net/
I hereby license the sound under the CC0 license, which can be found here:
https://creativecommons.org/publicdomain/zero/1.0/
