'''yay the player'''

from __future__ import division, print_function, unicode_literals

import math

import pygame
from pygame.locals import *

import data

# Movement constants
LEFT = 0
RIGHT = 1
UP = 2
DOWN = 3

PLAYER_SPEED = 3
PLAYER_SIZE = 24

class PlayerDrone:
    def __init__(self, x, y):
        # Resources
        self.alive_image = pygame.image.load(data.filepath("player_drone.png"))
        self.dead_image = pygame.image.load(data.filepath("player_dead.png"))

        self.pos = [x, y] # Top-left corner
        self.last_move = [0, 0] # For collision detection
        self.alive = True

        # left, right, up, down
        self.movement = [False, False, False, False]
        self.use_key = False

    def get_rect(self):
        return pygame.Rect(self.pos[0], self.pos[1], PLAYER_SIZE, PLAYER_SIZE)

    def kill(self):
        self.alive = False

        self.movement[LEFT] = False
        self.movement[RIGHT] = False
        self.movement[UP] = False
        self.movement[DOWN] = False

        self.use_key = False

    def on_pause(self):
        # So that the player doesn't continue moving on unpause
        # if the key is lifted later
        self.movement[LEFT] = False
        self.movement[RIGHT] = False
        self.movement[UP] = False
        self.movement[DOWN] = False

    def on_unpause(self):
        # So that the player starts moving on unpause instead
        # if the key is held before unpause instead of having
        # to lift up the key and press it again
        pressed = pygame.key.get_pressed()
        self.movement[LEFT] = pressed[K_LEFT]
        self.movement[RIGHT] = pressed[K_RIGHT]
        self.movement[UP] = pressed[K_UP]
        self.movement[DOWN] = pressed[K_DOWN]

    def read_event(self, event):
        if not self.alive:
            return

        if event.type == KEYDOWN:
            if event.key == K_LEFT:
                self.movement[LEFT] = True
            elif event.key == K_RIGHT:
                self.movement[RIGHT] = True
            elif event.key == K_UP:
                self.movement[UP] = True
            elif event.key == K_DOWN:
                self.movement[DOWN] = True

        elif event.type == KEYUP:
            if event.key == K_LEFT:
                self.movement[LEFT] = False
            elif event.key == K_RIGHT:
                self.movement[RIGHT] = False
            elif event.key == K_UP:
                self.movement[UP] = False
            elif event.key == K_DOWN:
                self.movement[DOWN] = False

            # TODO: make this a bit better
            elif event.key == K_SPACE:
                self.use_key = True

    def update(self):
        if not self.alive:
            self.last_move[0] = 0
            self.last_move[1] = 0
            return

        vector = [0, 0]

        if self.movement[LEFT]:
            vector[0] -= 1
        if self.movement[RIGHT]:
            vector[0] += 1
        if self.movement[UP]:
            vector[1] -= 1
        if self.movement[DOWN]:
            vector[1] += 1

        # No point continuing if there's no movement
        if vector[0] == 0 and vector[1] == 0:
            self.last_move[0] = 0
            self.last_move[1] = 0
            return

        # Get the magnitude of the vector
        mag = math.sqrt(vector[0] ** 2 + vector[1] ** 2)

        # Get a unit vector representing the direction of movement
        unit_vec = [vector[0] / mag, vector[1] / mag]

        # Multiply the unit vector by the player's speed
        vel = [unit_vec[0] * PLAYER_SPEED, unit_vec[1] * PLAYER_SPEED]

        # Move the player
        self.pos[0] += vel[0]
        self.pos[1] += vel[1]

        # Store the last movement for collision detection purposes
        self.last_move[0] = vel[0]
        self.last_move[1] = vel[1]

    def render(self, surf):
        img = self.alive_image if self.alive else self.dead_image
        surf.blit(img, self.pos)
