'''yay menus; this file is the epitome of copy and paste'''

from __future__ import division, print_function, unicode_literals

import pygame
from pygame.locals import *

from constants import WHITE, GRAY, DARK_TEAL, YELLOW, P_BLANK, P_TILE, \
                      GAME_HEIGHT, GAME_WIDTH, TILE_SIZE, F_BLANK, F_TILE
import data
import font
from level import PICKUP_SIZE
import sounds
import util

# For LevelSelectMenu
LEFT_MARGIN = 40
TOP_MARGIN = 120
LEVEL_BUTTON_WIDTH = 120
LEVEL_BUTTON_HEIGHT = 60
X_PADDING = 80
Y_PADDING = 30

# For DataRepairView
HEADER_HEIGHT = 100
FOOTER_HEIGHT = 220

# For WorldSelectMenu
WORLD = "world"
STORY = "story"

class MainMenu:
    def __init__(self, screen):
        self.screen = screen  # Should be a MainMenuScreen

        self.text_font = font.get_text_font()

        self.buttons = []
        self.music_button = None  # so that we can change the text
        self.sound_button = None
        self.mouse_down_pos = (0, 0)
        self.mouse_up_pos = (0, 0)
        self.current_hover = None

        self.title_image = pygame.image.load(data.filepath("title.png"))

        start_rect = pygame.Rect(300, 300, 200, 40)
        start_help = "Start the game."
        start_button = Button(self.text_font, "Start Game", start_help,
                              start_rect, self.screen.start_game)
        self.buttons.append(start_button)

        music_rect = pygame.Rect(300, 350, 200, 40)
        music_text = "Music: On" if self.screen.profile.play_music else "Music: Off"
        music_help = "Toggle music playback."
        music_button = Button(self.text_font, music_text, music_help,
                              music_rect, self.screen.toggle_music)
        self.buttons.append(music_button)
        self.music_button = music_button

        sound_rect = pygame.Rect(300, 400, 200, 40)
        sound_text = "Sound: On" if self.screen.profile.play_sound else "Sound: Off"
        sound_help = "Toggle sound playback."
        sound_button = Button(self.text_font, sound_text, sound_help,
                              sound_rect, self.screen.toggle_sound)
        self.buttons.append(sound_button)
        self.sound_button = sound_button

        quit_rect = pygame.Rect(300, 450, 200, 40)
        quit_help = "Exit the game."
        quit_button = Button(self.text_font, "Quit Game", quit_help,
                             quit_rect, self.screen.quit_game)
        self.buttons.append(quit_button)

        # No buttons are hidden in the main menu
        for button in self.buttons:
            button.active = True

    def read_event(self, event):
        if event.type == MOUSEMOTION:
            self.check_for_hover(event.pos)

        elif event.type == MOUSEBUTTONDOWN and event.button == 1:
            self.mouse_down_pos = event.pos

        elif event.type == MOUSEBUTTONUP and event.button == 1:
            self.mouse_up_pos = event.pos

            for button in self.buttons:
                # Make sure that the mouse down and mouse up took place in the
                # same button
                if (button.active and
                        button.rect.collidepoint(self.mouse_down_pos) and
                        button.rect.collidepoint(self.mouse_up_pos)):
                    button.hit()

    def check_for_hover(self, pos, no_sound=False):
        self.current_hover = None

        for button in self.buttons:
            # Play a sound only if it hasn't been hovered over yet
            # unless the mouse pointer ended up there when returning from
            # another menu
            play_sound = not button.hovered and not no_sound
            button.hovered = button.rect.collidepoint(pos)

            if button.hovered and button.active:
                self.current_hover = button

                if play_sound:
                    sounds.trigger_sound("button-hover")

    def render(self, surf):
        surf.blit(self.title_image, (100, 50))

        for button in self.buttons:
            button.render(surf)

        author_rect = pygame.Rect(580, 550, 220, 31)
        util.draw_frame(surf, author_rect)
        author_text = "A PyWeek #20 entry by Jjp137"
        font.draw_text(surf, self.text_font, author_text,
                       center=author_rect.center)

        help_rect = pygame.Rect(0, 580, 800, 20)
        help_text = "Use the left mouse button to select an item."
        help_pos = (2, 580)
        util.draw_frame(surf, help_rect)

        if self.current_hover is not None and self.current_hover.active:
            help_text = self.current_hover.help_text

        font.draw_text(surf, self.text_font, help_text, topleft=help_pos)

class WorldSelectMenu:
    def __init__(self, screen, master_file):
        self.screen = screen  # Should be a WorldSelectScreen

        self.text_font = font.get_text_font()
        self.big_font = font.get_big_font()

        self.buttons = []
        self.flag_dict = {} # string -> [Button]
        self.mouse_down_pos = (0, 0)
        self.mouse_up_pos = (0, 0)
        self.current_hover = None

        back_rect = pygame.Rect(10, 530, 100, 40)
        back_help = "Go back to the main menu."
        back_button = Button(self.text_font, "<- Back", back_help, back_rect,
                             self.screen.go_back)
        back_button.active = True
        self.buttons.append(back_button)

        self.parse_master_file(master_file)
        self.read_progress(self.screen.profile)

    def parse_master_file(self, master_file):
        fin = data.load(master_file, 'r')
        lines = [l.strip() for l in fin]
        fin.close()

        y_pos = 110
        for line in lines:
            info = line.split(',')

            title = info[0]
            action = info[1]
            file_name = info[2]
            unlock_flag = info[3]

            new_button = None
            help_text = None
            button_rect = pygame.Rect(10, y_pos, 780, 30)

            if action == STORY:
                help_text = "Read this chapter of the story."
                new_button = Button(self.text_font, title, help_text,
                    button_rect, self.screen.enter_story, file_name)

            elif action == WORLD:
                help_text = "Open the Select Device screen for this Data Set."
                new_button = Button(self.text_font, title, help_text,
                    button_rect, self.screen.enter_world, file_name)
            else:
                raise ValueError("invalid type in master.txt")

            self.buttons.append(new_button)

            if unlock_flag not in self.flag_dict:
                self.flag_dict[unlock_flag] = []
            self.flag_dict[unlock_flag].append(new_button)

            y_pos += 40

    def read_progress(self, prof):
        for flag, buttons in self.flag_dict.iteritems():
            for button in buttons:
                if flag.startswith("unlocked") or prof.is_world_done(flag):
                    button.active = True
                else:
                    button.active = False

    def read_event(self, event):
        if event.type == MOUSEMOTION:
            self.check_for_hover(event.pos)

        elif event.type == MOUSEBUTTONDOWN and event.button == 1:
            self.mouse_down_pos = event.pos

        elif event.type == MOUSEBUTTONUP and event.button == 1:
            self.mouse_up_pos = event.pos

            for button in self.buttons:
                # Make sure that the mouse down and mouse up took place in the
                # same button
                if (button.active and
                        button.rect.collidepoint(self.mouse_down_pos) and
                        button.rect.collidepoint(self.mouse_up_pos)):
                    button.hit()

    def check_for_hover(self, pos, no_sound=False):
        self.current_hover = None

        for button in self.buttons:
            # Play a sound only if it hasn't been hovered over yet
            # unless the mouse pointer ended up there when returning from
            # another menu
            play_sound = not button.hovered and not no_sound
            button.hovered = button.rect.collidepoint(pos)

            if button.hovered and button.active:
                self.current_hover = button

                if play_sound:
                    sounds.trigger_sound("button-hover")

    def render(self, surf):
        title_rect = pygame.Rect(10, 10, 780, 80)
        util.draw_frame(surf, title_rect)
        font.draw_text(surf, self.big_font, "Select Module", topleft=(20, 22))

        for button in self.buttons:
            button.render(surf)

        help_rect = pygame.Rect(0, 580, 800, 20)
        help_text = "Use the left mouse button to select an item."
        help_pos = (2, 580)
        util.draw_frame(surf, help_rect)

        if self.current_hover is not None and self.current_hover.active:
            help_text = self.current_hover.help_text

        font.draw_text(surf, self.text_font, help_text, topleft=help_pos)

class LevelSelectMenu:
    def __init__(self, screen):
        self.screen = screen  # Should be a LevelSelectScreen
        self.world = screen.world
        self.text_font = font.get_text_font()
        self.big_font = font.get_big_font()

        self.all_buttons = []
        self.story_button = None # need to set it to True later
        self.level_buttons = {} # subset of buttons; string -> Button
        self.mouse_down_pos = (0, 0)
        self.mouse_up_pos = (0, 0)
        self.current_hover = None

        self.fragment_count = 0
        self.keys_image = pygame.image.load(data.filepath("pickups.png"))
        self.keys_found = 0

        back_rect = pygame.Rect(10, 530, 100, 40)
        back_help = "Go back to the Module Select screen."
        back_button = Button(self.text_font, "<- Back", back_help, back_rect,
                             self.screen.go_back)
        back_button.active = True
        self.all_buttons.append(back_button)

        story_rect = pygame.Rect(400, 530, 180, 40)
        story_help = "Read a report on the repaired data."
        story_button = Button(self.text_font, "View Analysis", story_help,
                story_rect, self.screen.enter_story, self.world.story_file)
        story_button.active = False
        self.all_buttons.append(story_button)
        self.story_button = story_button

        repair_rect = pygame.Rect(610, 530, 180, 40)
        repair_help = "Enter Data Repair mode and put the fragments together."
        repair_button = Button(self.text_font, "Repair Data", repair_help,
                               repair_rect, self.screen.enter_repair)
        repair_button.active = True
        self.all_buttons.append(repair_button)

        level_help = "Send a tiny drone into this device."
        for entry in self.world.levels.itervalues():
            menu_x = entry.menu_coords[0]
            menu_y = entry.menu_coords[1]
            level_button_rect = get_level_button_rect(menu_x, menu_y)

            new_button = Button(self.text_font, entry.name, level_help,
                level_button_rect, self.screen.enter_level, entry.file_name)
            self.all_buttons.append(new_button)
            self.level_buttons[entry.unlock_flag] = new_button

        self.read_world_progress()

    # Updates the available level buttons
    def read_world_progress(self):
        for level in self.world.levels.itervalues():
            unlocked = level.is_unlocked(self.world.active_flags)
            self.level_buttons[level.unlock_flag].active = unlocked

            if level.unlock_flag in self.world.connectors:
                self.world.connectors[level.unlock_flag].active = unlocked

        self.fragment_count = 0
        for frag in self.world.fragments.itervalues():
            if frag.is_collected(self.world.active_flags):
                self.fragment_count += 1

        # Check if keys are found for UI purposes
        self.keys_found = 0
        for flag in self.world.active_flags:
            if flag.startswith("key"):
                self.keys_found += 1

        # Enable the story button if the world is complete
        if self.world.complete:
            self.story_button.active = True

    def read_event(self, event):
        if event.type == MOUSEMOTION:
            self.check_for_hover(event.pos)

        elif event.type == MOUSEBUTTONDOWN and event.button == 1:
            self.mouse_down_pos = event.pos

        elif event.type == MOUSEBUTTONUP and event.button == 1:
            self.mouse_up_pos = event.pos

            for button in self.all_buttons:
                # Make sure that the mouse down and mouse up took place in the
                # same button
                if (button.active and
                        button.rect.collidepoint(self.mouse_down_pos) and
                        button.rect.collidepoint(self.mouse_up_pos)):
                    button.hit()

    def check_for_hover(self, pos, no_sound=False):
        self.current_hover = None

        for button in self.all_buttons:
            # Play a sound only if it hasn't been hovered over yet
            # unless the mouse pointer ended up there when returning from
            # another menu
            play_sound = not button.hovered and not no_sound
            button.hovered = button.rect.collidepoint(pos)

            if button.hovered and button.active:
                self.current_hover = button

                if play_sound:
                    sounds.trigger_sound("button-hover")

    def render(self, surf):
        title_rect = pygame.Rect(10, 10, 780, 80)
        util.draw_frame(surf, title_rect)

        title = self.world.name + " :: Select Device"
        font.draw_text(surf, self.big_font, title, topleft=(20, 22))

        for button in self.all_buttons:
            button.render(surf)

        for connector in self.world.connectors.itervalues():
            connector.render(surf)

        if self.keys_found > 0:
            key_width = 85 + (self.keys_found * PICKUP_SIZE)
            key_rect = pygame.Rect(0, 470, key_width, 40)
            util.draw_frame(surf, key_rect)
            font.draw_text(surf, self.text_font, "Keys found:", topleft=(10, 480))

            key_nums = []
            for flag in self.world.active_flags:
                if flag.startswith("key"):
                    key_nums.append(int(flag.split('-')[1]))

            i = 0
            for num in sorted(key_nums):
                draw_pos = (i * PICKUP_SIZE + 85, 470 + 4)
                frame_rect = (PICKUP_SIZE * num, 0, PICKUP_SIZE, PICKUP_SIZE)

                surf.blit(self.keys_image, draw_pos, frame_rect)
                i += 1

        frag_rect = pygame.Rect(610, 470, 190, 40)
        util.draw_frame(surf, frag_rect)
        frag_text = ("Fragments found: " + str(self.fragment_count) + "/" +
                     str(len(self.world.fragments)))
        font.draw_text(surf, self.text_font, frag_text, center=frag_rect.center)

        help_rect = pygame.Rect(0, 580, 800, 20)
        help_text = "Use the left mouse button to select an item."
        help_pos = (2, 580)
        util.draw_frame(surf, help_rect)

        if self.current_hover is not None and self.current_hover.active:
            help_text = self.current_hover.help_text

        font.draw_text(surf, self.text_font, help_text, topleft=help_pos)

class StoryView:
    def __init__(self, screen, story_file):
        self.screen = screen  # Should be a StoryScreen

        self.text_font = font.get_text_font()
        self.big_font = font.get_big_font()

        self.title = None
        self.pages = []
        self.current_page = 0

        self.buttons = []
        self.prev_button = None  # so that we can show or hide them
        self.next_button = None
        self.mouse_down_pos = (0, 0)
        self.mouse_up_pos = (0, 0)
        self.current_hover = None

        prev_rect = pygame.Rect(190, 530, 100, 40)
        prev_help = "View the previous page."
        prev_button = Button(self.text_font, "<- Prev", prev_help, prev_rect,
                             self.prev_page)
        prev_button.active = False  # we always start on page 1
        self.buttons.append(prev_button)
        self.prev_button = prev_button

        next_rect = pygame.Rect(510, 530, 100, 40)
        next_help = "View the next page."
        next_button = Button(self.text_font, "Next ->", next_help, next_rect,
                             self.next_page)
        next_button.active = False  # wait until we parse the story file
        self.buttons.append(next_button)
        self.next_button = next_button

        back_rect = pygame.Rect(10, 530, 100, 40)
        back_help = "Go back to the main menu."
        back_button = Button(self.text_font, "<- Back", back_help, back_rect,
                             self.screen.go_back)
        back_button.active = True
        self.buttons.append(back_button)

        self.parse_story_file(story_file)

    def parse_story_file(self, file_name):
        # I don't know what exception handling is :p
        fin = data.load(file_name, 'r')
        lines = [l.strip() for l in fin]
        fin.close()

        self.title = lines[0].split('=')[1].strip()

        page = []
        for line in lines[1:]:
            if line == "[PAGE]":
                self.pages.append(page[:])
                page[:] = []
            else:
                page.append(line)

        # Append the last page
        self.pages.append(page)

        if len(self.pages) > 1:
            self.next_button.active = True

    def read_event(self, event):
        if event.type == MOUSEMOTION:
            self.check_for_hover(event.pos)

        elif event.type == MOUSEBUTTONDOWN and event.button == 1:
            self.mouse_down_pos = event.pos

        elif event.type == MOUSEBUTTONUP and event.button == 1:
            self.mouse_up_pos = event.pos

            for button in self.buttons:
                # Make sure that the mouse down and mouse up took place in the
                # same button
                if (button.active and
                        button.rect.collidepoint(self.mouse_down_pos) and
                        button.rect.collidepoint(self.mouse_up_pos)):
                    button.hit()

    def check_for_hover(self, pos, no_sound=False):
        self.current_hover = None

        for button in self.buttons:
            # Play a sound only if it hasn't been hovered over yet
            # unless the mouse pointer ended up there when returning from
            # another menu
            play_sound = not button.hovered and not no_sound
            button.hovered = button.rect.collidepoint(pos)

            if button.hovered and button.active:
                self.current_hover = button

                if play_sound:
                    sounds.trigger_sound("button-hover")

    def prev_page(self):
        self.current_page -= 1

        self.prev_button.active = self.current_page != 0
        self.next_button.active = self.current_page != len(self.pages) - 1

    def next_page(self):
        self.current_page += 1

        self.prev_button.active = self.current_page != 0
        self.next_button.active = self.current_page != len(self.pages) - 1

    def render(self, surf):
        title_rect = pygame.Rect(10, 10, 780, 80)
        util.draw_frame(surf, title_rect)

        font.draw_text(surf, self.big_font, self.title, topleft=(20, 22))

        for button in self.buttons:
            button.render(surf)

        story_rect = pygame.Rect(10, 110, 780, 400)
        util.draw_frame(surf, story_rect)

        # Draw the story here
        y_pos = 120
        for line in self.pages[self.current_page]:
            font.draw_text(surf, self.text_font, line, topleft=(20, y_pos))
            y_pos += 20

        page_rect = pygame.Rect(300, 530, 200, 40)
        util.draw_frame(surf, page_rect)
        page_text = ("Page " + str(self.current_page + 1) + "/" +
                     str(len(self.pages)))
        font.draw_text(surf, self.text_font, page_text, center=page_rect.center)

        help_rect = pygame.Rect(0, 580, 800, 20)
        help_text = "Read the document."
        help_pos = (2, 580)
        util.draw_frame(surf, help_rect)

        if self.current_hover is not None and self.current_hover.active:
            help_text = self.current_hover.help_text

        font.draw_text(surf, self.text_font, help_text, topleft=help_pos)

class DataRepairView: # probably horribly misnamed but oh well
    def __init__(self, screen, world):
        self.screen = screen # Should be a DataRepairScreen
        self.world = world

        self.grid = world.puzzle_grid  # An alias
        self.grid_rect = None
        self.origin = ()

        self.fragments = world.fragments  # Another alias
        self.frag_views = {}
        self.held_frag = None
        self.former_pos = None

        self.text_font = font.get_text_font()
        self.big_font = font.get_big_font()

        # So that we don't make the same surface over and over for highlighting
        # and previewing
        self.preview_surf = pygame.Surface((TILE_SIZE, TILE_SIZE))
        self.preview_coords = []
        self.preview_surf.set_alpha(128)

        self.highlight_surf = pygame.Surface((TILE_SIZE, TILE_SIZE))
        self.highlight_coords = []
        self.highlight_surf.set_alpha(128)
        self.highlight_frag = None

        self.toolbox_rect = pygame.Rect(0, 380, 800, 140)
        self.toolbox = []
        self.toolbox_offset = 0

        self.buttons = []
        self.prev_button = None  # so that the toolbox can update them
        self.next_button = None
        self.mouse_down_pos = (0, 0)
        self.mouse_up_pos = (0, 0)
        self.current_hover = None

        back_rect = pygame.Rect(10, 530, 100, 40)
        back_help = "Go back to the Select Device screen."
        back_button = Button(self.text_font, "<- Back", back_help, back_rect,
                             self.screen.go_back)
        back_button.active = True
        self.buttons.append(back_button)

        prev_rect = pygame.Rect(190, 530, 100, 40)
        prev_help = "Scroll the fragment list to the left."
        prev_button = Button(self.text_font, "<- Prev", prev_help, prev_rect,
                             self.scroll_left)
        prev_button.active = True
        self.buttons.append(prev_button)
        self.prev_button = prev_button

        next_rect = pygame.Rect(510, 530, 100, 40)
        next_help = "Scroll the fragment list to the right."
        next_button = Button(self.text_font, "Next ->", next_help, next_rect,
                             self.scroll_right)
        next_button.active = True
        self.buttons.append(next_button)
        self.next_button = next_button

        self.grid_img = pygame.image.load(data.filepath("grid.png"))

        # Center the grid
        left = (GAME_WIDTH / 2) - (TILE_SIZE * len(self.grid[0]) / 2)
        bound_area = GAME_HEIGHT - HEADER_HEIGHT - FOOTER_HEIGHT
        top = (bound_area / 2) - (TILE_SIZE * len(self.grid) / 2) + HEADER_HEIGHT

        self.origin = (int(left), int(top))

        grid_size = (TILE_SIZE * len(self.grid[0]), TILE_SIZE *  len(self.grid))
        self.grid_rect = pygame.Rect(self.origin, grid_size)

        # Add collected fragments to the list
        for frag in self.fragments.itervalues():
            if frag.is_collected(self.world.active_flags):
                new_view = FragmentView(frag)
                self.frag_views[frag.flag_name] = new_view

        self.load_from_profile(self.screen.profile)

    def load_from_profile(self, prof):
        frag_dict = prof.get_frag_pos(self.world.key)

        # For fragments that are already on the grid
        for flag, coords in frag_dict.iteritems():
            frag = self.frag_views[flag]
            frag.grid_pos = coords
            frag.in_grid = True
            frag.screen_pos = util.get_world_pos(self.origin,
                                                 coords[0], coords[1])

        for frag_view in self.frag_views.itervalues():
            # If it's not in the grid, it's in the toolbox
            if not frag_view.in_grid:
                self.toolbox.append(frag_view)

        self.update_toolbox(sort=True)

    def save_to_profile(self, prof):
        frag_dict = {}

        for frag in self.frag_views.itervalues():
            if frag.in_grid:
                frag_dict[frag.flag_name] = frag.grid_pos

        prof.update_frag_pos(self.world.key, frag_dict)
        prof.update_grid(self.world.key, self.grid)

        if self.world.complete:
            prof.mark_world_done(self.world.key)

    def read_event(self, event):
        # Clear these
        self.preview_coords[:] = []
        self.highlight_coords[:] = []

        if event.type == MOUSEMOTION:
            if self.held_frag is not None:
                self.held_frag.screen_pos[0] += event.rel[0]
                self.held_frag.screen_pos[1] += event.rel[1]

                self.create_preview()

            # Highlight pieces on the grid that the mouse pointer is over
            if not self.world.complete:
                self.create_highlight(event.pos)

            self.check_for_hover(event.pos)

        elif event.type == MOUSEBUTTONDOWN and event.button == 1:
            self.mouse_down_pos = event.pos

            if self.world.complete:
                return  # Don't drag any pieces if it's done

            for frag in self.frag_views.itervalues():
                if frag.renderable and frag.collidepoint(event.pos):
                    frag.held = True
                    self.held_frag = frag
                    self.former_pos = frag.screen_pos[:]
                    sounds.trigger_sound("frag-grab")

                    self.create_highlight(event.pos)
                    return # No more than one fragment

        elif event.type == MOUSEBUTTONUP and event.button == 1:
            self.mouse_up_pos = event.pos

            if self.held_frag is not None:
                self.attempt_placement()

                self.held_frag.held = False
                self.held_frag = None
                self.former_pos = None
                sounds.trigger_sound("frag-drop")

                if not self.world.complete:
                    self.create_highlight(event.pos)  # frag-on-grid hover
                    sounds.cancel_sound("button-hover")  # drop above a piece

            for button in self.buttons:
                # Make sure that the mouse down and mouse up took place in the
                # same button
                if (button.active and
                        button.rect.collidepoint(self.mouse_down_pos) and
                        button.rect.collidepoint(self.mouse_up_pos)):
                    button.hit()

    def create_preview(self):
        frag = self.held_frag
        frag_num = int(frag.flag_name.split('-')[1])
        snap_rect = frag.get_snap_rect(self.origin)
        closest_snap = self.calc_closest_snap(frag, snap_rect)

        # If at least one tile can be highlighted
        if self.grid_rect.colliderect(closest_snap):
            grid_coords = util.get_tile_pos(self.origin,
                                            closest_snap.x, closest_snap.y)

            # Avoid out of bounds by only checking placeability if
            # the snap rect is completely within the grid; otherwise assume
            # that the fragment can't be placed
            if self.grid_rect.contains(closest_snap):
                placeable = self.is_placeable(frag, frag_num, grid_coords)
            else:
                placeable = False

            self.preview_surf.fill((0, 0, 255) if placeable else (255, 0, 0))

            for frag_y, row in enumerate(frag.tiles):
                for frag_x, col in enumerate(row):
                    grid_x = grid_coords[0] + frag_x
                    grid_y = grid_coords[1] + frag_y

                    # Don't go out of bounds
                    if (grid_x < 0 or grid_x > len(self.grid[0]) - 1 or
                        grid_y < 0 or grid_y > len(self.grid) - 1):
                        continue

                    # Don't draw on blank tiles or where the fragment
                    # wouldn't fill anything
                    if (self.grid[grid_y][grid_x] == P_BLANK or
                        frag.tiles[frag_y][frag_x] == F_BLANK):
                        continue

                    screen_pos = util.get_world_pos(self.origin, grid_x, grid_y)
                    self.preview_coords.append(tuple(screen_pos))

    # The white alpha layer that appears over mouse-over'd pieces
    def create_highlight(self, point):
        self.highlight_surf.fill((255, 255, 255))

        for frag in self.frag_views.itervalues():
            # Highlight always if there's no held fragment; if there is a
            # held fragment, don't highlight anything else but the held one
            if (frag.collidepoint(point) and (self.held_frag is None or
                self.held_frag.flag_name == frag.flag_name)):
                for frag_y, row in enumerate(frag.tiles):
                    for frag_x, col in enumerate(row):
                        if frag.tiles[frag_y][frag_x] == F_TILE:
                            rect_pos = (frag.screen_pos[0] + TILE_SIZE * frag_x,
                                        frag.screen_pos[1] + TILE_SIZE * frag_y)
                            self.highlight_coords.append(tuple(rect_pos))

                if (self.highlight_frag is None or
                   (frag.flag_name != self.highlight_frag.flag_name)):
                    self.highlight_frag = frag
                    sounds.trigger_sound("button-hover")

                # Only one fragment should be highlighted
                return

        # If we got here, there's no highlighted frag
        self.highlight_frag = None

    def attempt_placement(self):
        frag = self.held_frag
        frag_num = int(frag.flag_name.split('-')[1])
        snap_rect = frag.get_snap_rect(self.origin)
        closest_snap = self.calc_closest_snap(frag, snap_rect)

        if self.grid_rect.contains(closest_snap):
            grid_coords = util.get_tile_pos(self.origin,
                                            closest_snap.x, closest_snap.y)

            if self.is_placeable(frag, frag_num, grid_coords):
                # Remove it from the toolbox
                if not frag.in_grid:
                    self.toolbox.remove(frag)
                    self.update_toolbox()

                frag.screen_pos = list(closest_snap.topleft)
                frag.grid_pos = list(grid_coords)
                frag.in_grid = True

                # Wipe out its former position first
                for grid_y, row in enumerate(self.grid):
                    for grid_x, col in enumerate(row):
                        if self.grid[grid_y][grid_x] == frag_num:
                            self.grid[grid_y][grid_x] = P_TILE

                # Then apply the new position
                for frag_y, row in enumerate(frag.tiles):
                    for frag_x, col in enumerate(row):
                        grid_x = grid_coords[0] + frag_x
                        grid_y = grid_coords[1] + frag_y

                        if frag.tiles[frag_y][frag_x] == F_TILE:
                            self.grid[grid_y][grid_x] = frag_num

                if self.check_completion():
                    sounds.trigger_sound("puzzle-finished")
                    self.world.complete = True

                return  # don't continue

        # It didn't succeed for whatever reason, so figure out a good
        # place for it

        # True when moved from grid to toolbox
        if (frag.in_grid
                and self.toolbox_rect.y < frag.get_rect(self.origin).centery):
            frag.grid_pos = None
            frag.in_grid = False

            for grid_y, row in enumerate(self.grid):
                for grid_x, col in enumerate(row):
                    if self.grid[grid_y][grid_x] == frag_num:
                        self.grid[grid_y][grid_x] = P_TILE

            self.toolbox.append(frag)
            self.update_toolbox()

        else:  # In cases of movement from grid/toolbox to invalid spot
            frag.screen_pos = self.former_pos[:]

    def is_placeable(self, frag, num, grid_coords):
        for frag_y, row in enumerate(frag.tiles):
            for frag_x, col in enumerate(row):
                grid_x = grid_coords[0] + frag_x
                grid_y = grid_coords[1] + frag_y

                # Can't place a piece of it on a non-tile
                if (self.grid[grid_y][grid_x] == P_BLANK and
                    frag.tiles[frag_y][frag_x] != F_BLANK):
                    return False

                # Can't overlap others (but it can replace itself)
                if (frag.tiles[frag_y][frag_x] == F_TILE and
                    self.grid[grid_y][grid_x] not in [P_TILE, num]):
                    return False

        # Placement is valid
        return True

    def calc_closest_snap(self, frag, topleft_snap):
        held_coords = frag.screen_pos

        snap_orig = topleft_snap.copy()

        snap_right = topleft_snap.copy()
        snap_right.x += TILE_SIZE

        snap_bottom = topleft_snap.copy()
        snap_bottom.y += TILE_SIZE

        snap_diagonal = topleft_snap.copy()
        snap_diagonal.x += TILE_SIZE
        snap_diagonal.y += TILE_SIZE

        snaps = [snap_orig, snap_right, snap_bottom, snap_diagonal]
        min_tuple = (None, None)  # rect, dist

        for rect in snaps:
            dist = util.distance(held_coords[0], held_coords[1],
                                 rect.x, rect.y)
            if min_tuple[0] is None or dist < min_tuple[1]:
                min_tuple = (rect, dist)

        return min_tuple[0]

    def check_for_hover(self, pos, no_sound=False):
        self.current_hover = None

        for button in self.buttons:
            # Play a sound only if it hasn't been hovered over yet
            # unless the mouse pointer ended up there when returning from
            # another menu
            play_sound = not button.hovered and not no_sound
            button.hovered = button.rect.collidepoint(pos)

            if button.hovered and button.active:
                self.current_hover = button

                if play_sound and self.held_frag is None:
                    sounds.trigger_sound("button-hover")

    def scroll_left(self):
        self.toolbox_offset -= 1
        self.update_toolbox()

    def scroll_right(self):
        self.toolbox_offset += 1
        self.update_toolbox()

    # call after any changes to the toolbox
    def update_toolbox(self, sort=False):
        def get_frag_key(f):
            return int(f.flag_name.split('-')[1])

        if sort:
            self.toolbox.sort(key=get_frag_key)

        # Special case: player removes the last item in the toolbox
        # and there are more than 6 items left
        if (self.toolbox_offset + 5 == len(self.toolbox) and
            self.toolbox_offset != 0):
            self.toolbox_offset -= 1

        x_pos = 20
        for i, frag in enumerate(self.toolbox):
            if i < self.toolbox_offset or i > self.toolbox_offset + 5:
                self.toolbox[i].renderable = False
                continue

            bound_rect = pygame.Rect(x_pos, 400, TILE_SIZE * 3, TILE_SIZE * 3)
            bound_rect.centery = self.toolbox_rect.centery

            frag.screen_pos = [bound_rect.x, bound_rect.y]
            frag.renderable = True

            x_pos += TILE_SIZE * 4

        self.prev_button.active = not self.toolbox_offset == 0
        self.next_button.active = self.toolbox_offset + 6 < len(self.toolbox)

    def check_completion(self):
        for grid_y, row in enumerate(self.grid):
            for grid_x, col in enumerate(row):
                if self.grid[grid_y][grid_x] == P_TILE:
                    return False

        return True

    def render(self, surf):
        # Render the header
        title_rect = pygame.Rect(10, 10, 780, 80)
        util.draw_frame(surf, title_rect)

        title = self.world.name + " :: Data Repair"
        font.draw_text(surf, self.big_font, title, topleft=(20, 22))

        for button in self.buttons:
            button.render(surf)

        for y, row in enumerate(self.grid):
            for x, col in enumerate(row):
                pos = util.get_world_pos(self.origin, x, y)

                if col != P_BLANK:
                    surf.blit(self.grid_img, pos)

        # Toolbox frame
        util.draw_frame(surf, self.toolbox_rect)

        if self.world.complete:
            complete_text = "Data repaired!"
            font.draw_text(surf, self.big_font, complete_text,
                           center=self.toolbox_rect.center)

        frag_rect = pygame.Rect(300, 530, 200, 40)
        util.draw_frame(surf, frag_rect)
        frag_text = ("Fragments found: " + str(len(self.frag_views)) + "/" +
                     str(len(self.world.fragments)))
        font.draw_text(surf, self.text_font, frag_text, center=frag_rect.center)

        # Current toolbox position
        page_rect = pygame.Rect(700, 519, 100, 25)
        util.draw_frame(surf, page_rect)
        first_num = 0 if len(self.toolbox) == 0 else self.toolbox_offset + 1
        second_num = 0 if len(self.toolbox) == 0 else min(len(self.toolbox), first_num + 5)
        page_text = str(first_num) + "-" + str(second_num) + " of " + str(len(self.toolbox))
        font.draw_text(surf, self.text_font, page_text, center=page_rect.center)

        help_rect = pygame.Rect(0, 580, 800, 20)
        if self.world.complete:
            help_text = "The data has been repaired."
        else:
            help_text = "Drag and drop the fragments onto the grid. "
            help_text += "Make sure each fragment fits perfectly."

        help_pos = (2, 580)
        util.draw_frame(surf, help_rect)

        if self.current_hover is not None and self.current_hover.active:
            help_text = self.current_hover.help_text

        font.draw_text(surf, self.text_font, help_text, help_pos)

        # First render pieces that are not being held
        for frag_view in filter(lambda v: not v.held,
                                self.frag_views.itervalues()):
            frag_view.render(surf)

        # Then draw the preview tiles
        for coords in self.preview_coords:
            surf.blit(self.preview_surf, coords)

        # The held piece is always above everything else
        if self.held_frag is not None:
            self.held_frag.render(surf)

        # ...unless it's highlighted
        for coords in self.highlight_coords:
            surf.blit(self.highlight_surf, coords)

class FragmentView:  # stuff used only in the data repair screen
    def __init__(self, frag):
        self.fragment = frag
        self.flag_name = frag.flag_name  # for the lazy
        self.tiles = frag.tiles  # ditto

        self.renderable = True
        self.in_grid = False  # if False, is being dragged from toolbox or in it
        self.held = False
        self.grid_pos = None # grid coords
        self.screen_pos = [0, 0]  # screen coords

    def row_len(self):
        return len(self.tiles[0])

    def col_len(self):
        return len(self.tiles)

    def get_rect(self, origin):
        dimensions = (TILE_SIZE * self.row_len(), TILE_SIZE * self.col_len())
        return pygame.Rect(self.screen_pos, dimensions)

    def get_snap_rect(self, origin):  # Always to the upperleft corner
        snap_pos = util.get_snap_pos(origin, self.screen_pos[0], self.screen_pos[1])
        dimensions = (TILE_SIZE * self.row_len(), TILE_SIZE * self.col_len())
        return pygame.Rect(snap_pos, dimensions)

    def collidepoint(self, point):
        if not self.renderable:
            return False

        return self.fragment.collidepoint(self.screen_pos, point)

    def render(self, surf):
        if not self.renderable:
            return

        self.fragment.render(surf, self.screen_pos)

class Button:
    def __init__(self, font, text, help_text, rect, func, *args):
        self.font = font
        self.text = text
        self.help_text = help_text
        self.rect = rect
        self.func = func
        self.args = args

        #self.ever_clicked = False
        self.active = False
        self.hovered = False

    def hit(self):
        #self.ever_clicked = True
        self.func(*self.args)
        sounds.trigger_sound("button-press")

    def render(self, surf):
        if not self.active:
            return

        frame_color = YELLOW if self.hovered else GRAY
        util.draw_frame(surf, self.rect, border_color=frame_color)
        font.draw_text(surf, self.font, self.text, center=self.rect.center)

def get_level_button_rect(menu_x, menu_y):
    x_pos = LEFT_MARGIN + ((LEVEL_BUTTON_WIDTH + X_PADDING) * menu_x)
    y_pos = TOP_MARGIN + ((LEVEL_BUTTON_HEIGHT + Y_PADDING) * menu_y)
    return pygame.Rect(x_pos, y_pos, LEVEL_BUTTON_WIDTH, LEVEL_BUTTON_HEIGHT)
