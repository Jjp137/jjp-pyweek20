'''font stuff yay'''

from __future__ import division, print_function, unicode_literals

import pygame

from constants import BLACK, WHITE
import data

def get_text_font():
    return pygame.font.Font(data.filepath("Gidole-Regular.ttf"), 16)

def get_big_font():
    return pygame.font.Font(data.filepath("Gidole-Regular.ttf"), 48)

def draw_text(surf, font, text, topleft=None, center=None, color=WHITE):
    font_surf = font.render(text, True, color)

    font_rect = font_surf.get_rect()

    if topleft is not None:
        font_rect.topleft = topleft
    if center is not None:
        font_rect.center = center

    surf.blit(font_surf, font_rect)

# Returns (font_surf, font_rect)
# If you need the rect for whatever reason, use this instead
def get_font_tuple(font, text, color=WHITE):
    font_surf = font.render(text, True, color)

    return (font_surf, font_surf.get_rect())
