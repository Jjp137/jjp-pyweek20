'''constants used in more than one file'''

GAME_WIDTH = 800
GAME_HEIGHT = 600

TILE_SIZE = 32

WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
DARK_TEAL = (19, 48, 48)
GRAY = (128, 128, 128)
YELLOW = (255, 255, 0)

# For the puzzle grid
P_BLANK = -2 #
P_TILE = -1 #

F_BLANK = '.'
F_TILE = '#'
