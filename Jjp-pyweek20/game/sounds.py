'''sound and music playback'''

from __future__ import division, print_function, unicode_literals

import pygame

import data

MUSIC_FILES = {"echoes": ("echoes_of_time.ogg", 0.7),
               "junkyard": ("junkyard_tribe.ogg", 0.5)
              }

class MusicPlayer:
    def __init__(self, profile):
        self.profile = profile

        self.current_song = None

    def play_song(self, song):
        if not self.profile.play_music:
            return

        if song not in MUSIC_FILES:
            raise ValueError("given song is bad: " + song)

        # Don't restart the song
        if song is not None and song == self.current_song:
            return
        else:
            self.current_song = song

            file_name = MUSIC_FILES[song][0]
            volume = MUSIC_FILES[song][1]
            pygame.mixer.music.load(data.filepath(file_name))
            pygame.mixer.music.set_volume(0.5)
            pygame.mixer.music.play(-1)

    def stop_music(self):
        if self.current_song is None:
            return

        self.current_song = None
        pygame.mixer.music.stop()

# I'm lazy so the sound system is global; sorry!

all_sounds = {}
sound_bools = {}
active_profile = None

# Call after pygame.init()
def load_sounds():
    global all_sounds, sound_bools

    def load(name):
        return pygame.mixer.Sound(data.filepath(name))

    all_sounds = {"button-hover": load("button-hover.ogg"),
                  "button-press": load("button-press.ogg"),
                  "escape-key": load("escape-key.ogg"),
                  "frag-grab": load("frag-grab.ogg"),
                  "frag-drop": load("frag-drop.ogg"),
                  "puzzle-finished": load("puzzle-finished.ogg"),
                  "entry-port": load("entry-port.ogg"),
                  "fire": load("fire.ogg"),
                  "enemy-attack": load("enemy-attack.ogg"),
                  "item-pickup": load("item-pickup.ogg"),
                  "node-activate": load("node-activate.ogg"),
                  "respawn": load("respawn.ogg"),
                  "walls-lower": load("walls-lower.ogg")
                 }

    sound_bools = {"button-hover": False,
                   "button-press": False,
                   "escape-key": False,
                   "frag-grab": False,
                   "frag-drop": False,
                   "puzzle-finished": False,
                   "entry-port": False,
                   "fire": False,
                   "enemy-attack": False,
                   "item-pickup": False,
                   "node-activate": False,
                   "respawn": False,
                   "walls-lower": False
                  }

# ...and this too
def set_profile(prof):
    global active_profile
    active_profile = prof

# Sets its boolean to True
def trigger_sound(key):
    if not active_profile.play_sound:
        return

    if key not in all_sounds:
        raise ValueError("given sound is bad: " + key)

    sound_bools[key] = True

def cancel_sound(key):
    if key not in all_sounds:
        raise ValueError("given sound is bad: " + key)

    sound_bools[key] = False

# Call once per frame
def play_sounds():
    if not active_profile.play_sound:
        return

    for key in sound_bools:
        if sound_bools[key]:
            all_sounds[key].play()

        sound_bools[key] = False
