'''a LevelGroup, or "world" as most games call it'''

from __future__ import division, print_function, unicode_literals

import pygame

import data
from constants import WHITE, P_BLANK, P_TILE, TILE_SIZE
from level import Level
from menus import get_level_button_rect
import util

BLANK = '_'
NOT_BLANK = "."

# Fragment colors
RED_FRAG = 0
BLUE_FRAG = 1
YELLOW_FRAG = 2
GREEN_FRAG = 3
PURPLE_FRAG = 4
GRAY_FRAG = 5

# Filename mapping
frag_images = {RED_FRAG : "red_square.png",
               BLUE_FRAG : "blue_square.png",
               YELLOW_FRAG : "yellow_square.png",
               GREEN_FRAG : "green_square.png",
               PURPLE_FRAG : "purple_square.png",
               GRAY_FRAG: "gray_square.png"}

class LevelGroup:  # also known as "world" within the code, sorry about that
    def __init__(self, profile, world_file):
        self.name = None
        self.key = None  # used by levels to write progress to the world
        self.story_file = None
        self.levels = {}  # string -> LevelEntry
        self.fragments = {} # string -> Fragment
        self.puzzle_grid = []
        self.connectors = {} # string -> Connector
        self.active_flags = set()  # flags activated by the player
        self.complete = False

        # Flags must be mentioned in the world file before they can be used
        # in levels; otherwise an error should occur
        self.valid_flags = set()

        # TODO: exception handling
        fin = data.load(world_file, 'r')
        lines = [line.strip() for line in fin]
        fin.close()

        self.name = lines[0].split('=')[1].strip()
        self.key = lines[1].split('=')[1].strip()
        self.story_file = lines[2].split('=')[1].strip()

        level_begin = lines.index("[LEVELS]") + 1
        level_end = lines.index("[KEYS]")

        for entry in lines[level_begin:level_end]:
            new_entry = LevelEntry(entry)
            self.levels[new_entry.name] = new_entry
            self.valid_flags.add(new_entry.unlock_flag)

        keys_begin = lines.index("[KEYS]") + 1
        keys_end = lines.index("[FRAGMENTS]")

        for key in lines[keys_begin:keys_end]:
            self.valid_flags.add(key)

        frags_begin = lines.index("[FRAGMENTS]") + 1
        frags_end = lines.index("[GRID]")

        for fragment in lines[frags_begin:frags_end]:
            new_frag = Fragment(fragment)
            self.fragments[new_frag.flag_name] = new_frag
            self.valid_flags.add(new_frag.flag_name)

        grid_begin = lines.index("[GRID]") + 1
        grid_end = lines.index("[CONNECTORS]")

        # I could barely understand this expression myself so lol
        self.puzzle_grid = [[int(i) for i in l.split(' ')]
                             for l in lines[grid_begin:grid_end]]

        con_begin = lines.index("[CONNECTORS]") + 1
        con_end = len(lines)

        for con in lines[con_begin:con_end]:
            new_con = Connector(con)
            self.connectors[new_con.display_flag] = new_con

            # Make sure that I didn't typo a flag name or something
            if new_con.display_flag not in self.valid_flags:
                raise ValueError("Bad connector key: " + new_con.display_flag)

        # Read profile to see if any flags should be already activated
        # and for the saved state of the grid
        self.read_save_data(profile)

    def read_save_data(self, profile):
        # Create the dict entry if it does not exist already
        if self.key not in profile.world_data:
            profile.init_world_data(self)

        # Read from the profile and apply differences to the world
        # Note: you can probably craft a bad save file if you really wanted to
        # but oh well :\
        else:
            self.active_flags = profile.get_flags(self.key)
            self.puzzle_grid = profile.get_grid(self.key)
            self.complete = profile.is_world_done(self.key)

            for flag in self.active_flags:
                if flag not in self.valid_flags:
                    print("Warning: invalid flag: " + flag)

class LevelEntry:
    def __init__(self, line):
        # data line: name = filename x,y flag
        self.name = None
        self.file_name = None
        self.menu_coords = None # 0 to 3
        self.unlock_flag = None

        temp = line.split('=')
        self.name = temp[0].strip()

        # One consequence of this is that file names can't have spaces
        rest = temp[1].strip().split(' ')
        coords = rest[1].split(',')

        self.file_name = rest[0]
        self.menu_coords = (int(coords[0]), int(coords[1]))
        self.unlock_flag = rest[2]

    def is_unlocked(self, flags):
        if self.unlock_flag.startswith("unlocked"): # Unlocked by default
            return True

        return self.unlock_flag in flags

class Fragment:
    def __init__(self, line):
        self.flag_name = None
        self.image = None
        self.tiles = []

        temp = line.split(' ')
        self.flag_name = temp[0]

        file_name = frag_images[int(temp[1])]
        self.image = pygame.image.load(data.filepath(file_name))

        for row in temp[2].split('|'):
            self.tiles.append(row)

        # Check that every row is the same length
        length = len(self.tiles[0])
        for row in self.tiles:
            if len(row) != length:
                raise ValueError("A row in fragment with flag " +
                                  self.flag_name + " is malformed.")

    def is_collected(self, flags):
        return self.flag_name in flags

    def collidepoint(self, origin, point):
        for y, row in enumerate(self.tiles):
            for x, col in enumerate(row):
                if col == '#':
                    tile_pos = util.get_world_pos(origin, x, y)
                    tile_rect = pygame.Rect(tile_pos[0], tile_pos[1],
                        TILE_SIZE, TILE_SIZE)

                    if tile_rect.collidepoint(point):
                        return True

        return False

    def render(self, surf, topleft):
        for y, row in enumerate(self.tiles):
            for x, col in enumerate(row):
                if col == '#':
                    pos = (topleft[0] + (x * TILE_SIZE),
                           topleft[1] + (y * TILE_SIZE))

                    surf.blit(self.image, pos)

# Used by LevelSelectScreen; idk why it's in here though
class Connector:
    def __init__(self, line):
        self.start_pos = None
        self.end_pos = None
        self.display_flag = None

        self.active = False

        temp = line.split(' ')
        start = temp[0].split(',')
        end = temp[1].split(',')

        start_rect = get_level_button_rect(int(start[0]), int(start[1]))
        end_rect = get_level_button_rect(int(end[0]), int(end[1]))

        if start_rect.x < end_rect.x:
            self.start_pos = start_rect.midright
            self.end_pos = end_rect.midleft
        elif start_rect.x > end_rect.x:
            self.start_pos = start_rect.midleft
            self.end_pos = end_rect.midright
        elif start_rect.y < end_rect.y:
            self.start_pos = start_rect.midbottom
            self.end_pos = end_rect.midtop
        elif start_rect.y > end_rect.y:
            self.start_pos = start_rect.midtop
            self.end_pos = end_rect.midbottom
        else:
            raise ValueError("coords are the same")

        self.display_flag = temp[2].strip()

    # Called by the level select menu
    def render(self, surf):
        if not self.active:
            return

        pygame.draw.line(surf, WHITE, self.start_pos, self.end_pos, 1)
