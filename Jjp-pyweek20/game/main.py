'''Game main module.

Contains the entry point used by the run_game.py script.

Feel free to put all your game code here, or in other modules in this "gamelib"
package.
'''

from __future__ import division, print_function, unicode_literals

import pygame
from pygame.locals import *

from constants import BLACK, GAME_HEIGHT, GAME_WIDTH
import data
from screens import ScreenStack, MainMenuScreen
import sounds
from profile import Profile, load_profile, save_profile

FPS = 60  # game logic fps and max rendering cap
FRAME_TIME = (1.0 / FPS) * 1000  # time for each game logic update in ms

def main():
    game_loop()

def game_loop():
    pygame.init()
    sounds.load_sounds()
    clock = pygame.time.Clock()

    window = pygame.display.set_mode((GAME_WIDTH, GAME_HEIGHT))
    pygame.display.set_caption("Fragmented ~A Dying Land~")

    frame_lag = 0.0

    # Attempt to load the profile here
    prof = load_profile()
    if prof is None:
        prof = Profile()

    sounds.set_profile(prof)

    screen_stack = ScreenStack(prof)
    screen_stack.push_screen(MainMenuScreen(screen_stack))

    while True:
        frame_lag += clock.tick_busy_loop()

        while frame_lag >= FRAME_TIME:
            screen_stack.read_input(pygame.event.get())

            # Quit the game if requested
            if not screen_stack.running:
                save_profile(prof)
                pygame.quit()
                return

            screen_stack.update()

            frame_lag -= FRAME_TIME

        window.fill(BLACK)

        screen_stack.render(window)
        sounds.play_sounds()

        pygame.display.update()
        pygame.time.delay(int(FRAME_TIME - frame_lag)) # 60 fps rendering cap
        #print(clock.get_fps())
