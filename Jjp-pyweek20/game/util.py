'''obligatory util library'''

from __future__ import division, print_function, unicode_literals

from constants import TILE_SIZE, DARK_TEAL, GRAY

import math
import pygame

def get_world_pos(origin, tile_x, tile_y):
    return [origin[0] + (TILE_SIZE * tile_x), origin[1] + (TILE_SIZE * tile_y)]

def get_tile_pos(origin, world_x, world_y):
    return [(world_x - origin[0]) // TILE_SIZE, (world_y - origin[1]) // TILE_SIZE]

# Get a world pos that is the nearest topleft corner of a grid square
def get_snap_pos(origin, world_x, world_y):
    raw_x = world_x - origin[0]
    raw_y = world_y - origin[1]

    return [origin[0] + (raw_x // TILE_SIZE * TILE_SIZE),
            origin[1] + (raw_y // TILE_SIZE * TILE_SIZE)]

def distance(x1, y1, x2, y2):
    return math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)

# For getting the position of topleft needed to center an item's position
# at a given tile
def center_pos_helper(origin, item_size, tile_x, tile_y):
    result_coords = get_world_pos(origin, tile_x, tile_y)
    result_coords[0] += (TILE_SIZE - item_size) // 2
    result_coords[1] += (TILE_SIZE - item_size) // 2

    return result_coords

def draw_frame(surf, rect, color=DARK_TEAL, border_color=GRAY):
    pygame.draw.rect(surf, color, rect)
    pygame.draw.rect(surf, border_color, rect, 1)
