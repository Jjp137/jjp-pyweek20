'''Some module with screens in it.'''

from __future__ import division, print_function, unicode_literals

from random import randint

import sys

import pygame
from pygame.locals import *

import data
from level import Level
from level_group import LevelGroup
import font
from menus import MainMenu, WorldSelectMenu, LevelSelectMenu, DataRepairView, \
                  StoryView
import sounds
from sounds import MusicPlayer
from profile import save_profile

MASTER_FILE = "master.txt"

class ScreenStack:
    def __init__(self, profile):
        self.screens = []
        self.profile = profile
        self.music = MusicPlayer(profile)
        self.running = True

    def push_screen(self, screen):
        self.screens.append(screen)

    def pop_screen(self):
        self.screens.pop()

        if len(self.screens) != 0:
            self.screens[-1].on_unhide()

    def read_input(self, events):
        if len(self.screens) == 0:
            raise NoScreenError("read_input(): No screen to send input to")

        self.screens[-1].read_input(events)

    def update(self):
        if len(self.screens) == 0:
            raise NoScreenError("update(): No screen to update")

        self.screens[-1].update()

    def render(self, surf):
        if len(self.screens) == 0:
            raise NoScreenError("render(): No screen to render")

        self.screens[-1].render(surf)

class Screen:
    def __init__(self, stack):
        self.screen_stack = stack
        self.profile = stack.profile
        self.music = stack.music

    def on_unhide(self):
        raise NotImplementedError("Screen.on_unhide()")

    def read_input(self, events):
        raise NotImplementedError("Screen.read_input(events)")

    def update(self):
        raise NotImplementedError("Screen.update()")

    def render(self, surf):
        raise NotImplementedError("Screen.render(surf)")

class MainMenuScreen(Screen):
    def __init__(self, stack):
        Screen.__init__(self, stack)

        self.background = pygame.image.load(data.filepath("background.png"))
        self.main_menu = MainMenu(self)

        # Start the music
        self.music.play_song("echoes")

    def on_unhide(self):
        # Hack to un-highlight the clicked button on return to this screen
        # and highlight the button that the mouse pointer happened to be on
        mouse_pos = pygame.mouse.get_pos()
        cur_hover = self.main_menu.current_hover

        if cur_hover is not None and cur_hover.rect.collidepoint(mouse_pos):
            cur_hover.hovered = False

        self.main_menu.check_for_hover(mouse_pos, True)

    def read_input(self, events):
        for event in events:
            if event.type == QUIT:
                self.screen_stack.running = False
                return

            if event.type == KEYUP and event.key == K_ESCAPE:
                self.quit_game()
                return

            self.main_menu.read_event(event)

    # Used in MainMenu
    def start_game(self):
        self.screen_stack.push_screen(
            WorldSelectScreen(self.screen_stack, MASTER_FILE))

    # Used in MainMenu
    def toggle_music(self):
        if self.profile.play_music:
            self.profile.play_music = False
            self.music.stop_music()

            self.main_menu.music_button.text = "Music: Off"
        else:
            self.profile.play_music = True
            self.music.play_song("echoes")

            self.main_menu.music_button.text = "Music: On"

    # Used in MainMenu
    def toggle_sound(self):
        if self.profile.play_sound:
            self.profile.play_sound = False
            self.main_menu.sound_button.text = "Sound: Off"
        else:
            self.profile.play_sound = True
            self.main_menu.sound_button.text = "Sound: On"

    # Used in MainMenu
    def quit_game(self):
        # main.py already saves the game upon quit
        self.screen_stack.running = False

    def update(self):
        pass

    def render(self, surf):
        surf.blit(self.background, (0, 0))

        self.main_menu.render(surf)

class WorldSelectScreen(Screen):
    def __init__(self, stack, master_file):
        Screen.__init__(self, stack)

        self.background = pygame.image.load(data.filepath("background.png"))
        self.world_menu = WorldSelectMenu(self, master_file)

        # Highlight the button that the cursor lands on
        mouse_pos = pygame.mouse.get_pos()
        self.world_menu.check_for_hover(mouse_pos, True)

    def on_unhide(self):
        self.world_menu.read_progress(self.profile)

        # Hack to un-highlight the clicked button on return to this screen
        # and highlight the button that the mouse pointer happened to be on
        mouse_pos = pygame.mouse.get_pos()
        cur_hover = self.world_menu.current_hover

        if cur_hover is not None and cur_hover.rect.collidepoint(mouse_pos):
            cur_hover.hovered = False

        self.world_menu.check_for_hover(mouse_pos, True)

    def read_input(self, events):
        for event in events:
            if event.type == QUIT:
                self.screen_stack.running = False
                return

            if event.type == KEYUP and event.key == K_ESCAPE:
                sounds.trigger_sound("escape-key")
                self.go_back()
                return

            self.world_menu.read_event(event)

    # Used in WorldSelectMenu
    def enter_world(self, file_name):
        self.screen_stack.push_screen(
            LevelSelectScreen(self.screen_stack, file_name))

    # Used in WorldSelectMenu
    def enter_story(self, file_name):
        self.screen_stack.push_screen(
            StoryScreen(self.screen_stack, file_name))

    # Used in WorldSelectMenu
    def go_back(self):
        save_profile(self.profile)
        self.screen_stack.pop_screen()

    def update(self):
        pass

    def render(self, surf):
        surf.blit(self.background, (0, 0))

        self.world_menu.render(surf)

class LevelSelectScreen(Screen):
    def __init__(self, stack, world_file):
        Screen.__init__(self, stack)

        self.background = pygame.image.load(data.filepath("background.png"))

        self.world = LevelGroup(self.profile, world_file)
        self.level_menu = LevelSelectMenu(self)

        # Highlight the button that the cursor lands on
        mouse_pos = pygame.mouse.get_pos()
        self.level_menu.check_for_hover(mouse_pos, True)

    def on_unhide(self):
        self.world.read_save_data(self.profile)
        self.level_menu.read_world_progress()

        # Hack to un-highlight the clicked button on return to this screen
        # and highlight the button that the mouse pointer happened to be on
        mouse_pos = pygame.mouse.get_pos()
        cur_hover = self.level_menu.current_hover

        if cur_hover is not None and cur_hover.rect.collidepoint(mouse_pos):
            cur_hover.hovered = False

        self.level_menu.check_for_hover(mouse_pos, True)

    def read_input(self, events):
        for event in events:
            if event.type == QUIT:
                self.screen_stack.running = False
                return

            if event.type == KEYUP and event.key == K_ESCAPE:
                sounds.trigger_sound("escape-key")
                self.go_back()
                return

            self.level_menu.read_event(event)

    # Used in LevelSelectMenu
    def enter_level(self, file_name):
        self.music.play_song("junkyard")
        self.screen_stack.push_screen(
            LevelScreen(self.screen_stack, file_name))

    # Used in LevelSelectMenu
    def enter_repair(self):
        self.music.play_song("echoes")
        self.screen_stack.push_screen(
            DataRepairScreen(self.screen_stack, self.world))

    # Used in LevelSelectMenu
    def enter_story(self, file_name):
        self.music.play_song("echoes")
        self.screen_stack.push_screen(
            StoryScreen(self.screen_stack, file_name))

    def go_back(self):
        self.music.play_song("echoes")
        save_profile(self.profile)
        self.screen_stack.pop_screen()

    def update(self):
        pass

    def render(self, surf):
        surf.blit(self.background, (0, 0))

        self.level_menu.render(surf)

class LevelScreen(Screen):
    def __init__(self, stack, level_file):
        Screen.__init__(self, stack)

        self.background = pygame.image.load(data.filepath("background.png"))

        self.level = Level(level_file, self.profile)

        sounds.trigger_sound("respawn")

    def on_unhide(self):
        pass

    def read_input(self, events):
        for event in events:
            if event.type == QUIT:
                self.screen_stack.running = False
                return

            if event.type == KEYUP and event.key == K_ESCAPE and self.level.paused:
                self.music.play_song("echoes")  # not junkyard; intentional
                sounds.trigger_sound("escape-key")
                self.screen_stack.pop_screen()

            else:
                self.level.read_event(event)

    def update(self):
        self.level.update()

        if self.level.exiting:
            self.level.write_to_profile(self.profile)
            save_profile(self.profile)
            self.screen_stack.pop_screen()

    def render(self, surf):
        surf.blit(self.background, (0, 0))

        self.level.render(surf)

class StoryScreen(Screen):
    def __init__(self, stack, story_file):
        Screen.__init__(self, stack)

        self.background = pygame.image.load(data.filepath("background.png"))
        self.view = StoryView(self, story_file)

        # Highlight the button that the cursor lands on
        mouse_pos = pygame.mouse.get_pos()
        self.view.check_for_hover(mouse_pos, True)

    def on_unhide(self):
        pass

    def read_input(self, events):
        for event in events:
            if event.type == QUIT:
                self.screen_stack.running = False
                return

            if event.type == KEYUP and event.key == K_ESCAPE:
                sounds.trigger_sound("escape-key")
                self.go_back()
                return

            self.view.read_event(event)

    # Used in StoryView
    def go_back(self):
        self.screen_stack.pop_screen()

    def update(self):
        pass

    def render(self, surf):
        surf.blit(self.background, (0, 0))

        self.view.render(surf)

class DataRepairScreen(Screen):
    def __init__(self, stack, world):
        Screen.__init__(self, stack)

        self.background = pygame.image.load(data.filepath("background.png"))

        self.world = world
        self.view = DataRepairView(self, self.world)

        # Highlight the button that the cursor lands on
        mouse_pos = pygame.mouse.get_pos()
        self.view.check_for_hover(mouse_pos, True)

    def on_unhide(self):
        pass

    def read_input(self, events):
        for event in events:
            if event.type == QUIT:
                self.screen_stack.running = False
                return

            if event.type == KEYUP and event.key == K_ESCAPE:
                sounds.trigger_sound("escape-key")
                self.go_back()
                return

            self.view.read_event(event)

    # Used in DataRepairView
    def go_back(self):
        self.view.save_to_profile(self.profile)
        save_profile(self.profile)
        self.screen_stack.pop_screen()

    def update(self):
        pass

    def render(self, surf):
        surf.blit(self.background, (0, 0))

        self.view.render(surf)

class NoScreenError(Exception):
    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return repr(self.msg)
