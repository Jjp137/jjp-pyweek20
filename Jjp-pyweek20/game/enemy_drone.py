'''Yay enemies.'''
from __future__ import division, print_function, unicode_literals

import math

import pygame

from constants import TILE_SIZE
import data
import util

# Direction constants
LEFT = 0
RIGHT = 1
UP = 2
DOWN = 3

ENEMY_SIZE = 24

class EnemyDrone:
    def __init__(self, level_origin, path_data):
        # Resources
        self.drone_image = pygame.image.load(data.filepath("enemy_drone.png"))

        self.pos = [0, 0]
        self.level_origin = level_origin

        self.path_nodes = []
        self.current_objective = None
        self.direction = None
        self.speed = None

        self.setup_path(path_data)
        self.calculate_next_direction()

    def setup_path(self, path_data):
        temp = path_data.split(' ')

        path_points = temp[0].split('->')
        self.speed = int(temp[1][0])

        for point in path_points:
            coords = point.split(',')
            dest = util.center_pos_helper(self.level_origin, ENEMY_SIZE,
                                           int(coords[0]), int(coords[1]))
            self.path_nodes.append(tuple(dest))

        first_node = self.path_nodes[0]
        self.pos[0] = first_node[0]
        self.pos[1] = first_node[1]

        # 0 is the starting position, will become 1
        self.current_objective = 0

    def get_rect(self):
        return pygame.Rect(self.pos[0], self.pos[1], ENEMY_SIZE, ENEMY_SIZE)

    def reset(self):
        first_node = self.path_nodes[0]
        self.pos[0] = first_node[0]
        self.pos[1] = first_node[1]

        self.current_objective = 0

        self.calculate_next_direction()

    def update(self):
        steps = self.speed

        # Handle it step by step b/c it might turn in the middle of the
        # update process
        while steps > 0:
            if self.direction == LEFT:
                self.pos[0] -= 1
            elif self.direction == RIGHT:
                self.pos[0] += 1
            elif self.direction == UP:
                self.pos[1] -= 1
            elif self.direction == DOWN:
                self.pos[1] += 1

            dest = self.path_nodes[self.current_objective]
            if self.pos[0] == dest[0] and self.pos[1] == dest[1]:
                self.calculate_next_direction()

            steps -= 1

    # called upon init and upon arrival at a node
    def calculate_next_direction(self):
        next_index = (self.current_objective + 1) % len(self.path_nodes)
        cur_node = self.path_nodes[self.current_objective]
        next_node = self.path_nodes[next_index]

        if next_node[0] < cur_node[0]:
            self.direction = LEFT
        elif next_node[0] > cur_node[0]:
            self.direction = RIGHT
        elif next_node[1] < cur_node[1]:
            self.direction = UP
        elif next_node[1] > cur_node[1]:
            self.direction = DOWN
        else:
            raise ValueError("check enemy_drone.py")

        self.current_objective = next_index

    def render(self, surf):
        surf.blit(self.drone_image, self.pos)
