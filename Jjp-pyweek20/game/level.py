'''Yay levels.'''

from __future__ import division, print_function, unicode_literals

from random import randint

import pygame
from pygame.locals import *

from constants import GAME_HEIGHT, GAME_WIDTH, TILE_SIZE
import data
from enemy_drone import EnemyDrone
import font
from player_drone import PlayerDrone, PLAYER_SIZE
import sounds
import util

PLAYER_START = '@'
WALL = '#'
FLOOR = '.'
FIRE = '^'
BLANK = '_'
LOCK_TILES = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']

PICKUP_SIZE = 32  # really only for fragments
KEY_OFFSET_X = 6
KEY_OFFSET_Y = 2
KEY_HITBOX_X = 20
KEY_HITBOX_Y = 28

NODE_SIZE = 24
NODE_OFFSET = 4
NODE_DETECT = 48

# Node state
IDLE = 0
ACTIVATING = 1
DEACTIVATING = 2

# Item types
KEY_ZERO = 0
KEY_ONE = 1
KEY_TWO = 2
KEY_THREE = 3
KEY_FOUR = 4
KEY_FIVE = 5
KEY_SIX = 6
KEY_SEVEN = 7
KEY_EIGHT = 8
KEY_NINE = 9
KEYS = [KEY_ZERO, KEY_ONE, KEY_TWO, KEY_THREE, KEY_FOUR, KEY_FIVE,
        KEY_SIX, KEY_SEVEN, KEY_EIGHT, KEY_NINE]
DATA_FRAGMENT = 10

class Level:
    def __init__(self, level_file, profile):
        # Resources
        self.wall_tile = pygame.image.load(data.filepath("wall.png"))
        self.floor_tile = pygame.image.load(data.filepath("floor.png"))
        self.firewall_tile = pygame.image.load(data.filepath("firewall.png"))
        self.start_tile = pygame.image.load(data.filepath("entry_port.png"))
        self.locked_tiles = pygame.image.load(data.filepath("lock_tiles.png"))

        self.font = font.get_text_font()

        self.name = None
        self.world_key = None
        self.tiles = []
        self.origin = ()
        self.player = None
        self.start_point = None
        self.enemies = []
        self.nodes = []
        self.pickups = []

        self.event_text = None
        self.text_timer = 0
        self.paused = False

        self.flags_triggered = set()  # For this session only
        self.unlocked_tiles = {} # This session only, coords -> tile
        self.exiting = False

        # TODO: exception handling
        fin = data.load(level_file, 'r')
        lines = [line.strip() for line in fin]
        fin.close()

        self.name = lines[0].split('=')[1].strip()
        self.world_key = lines[1].split('=')[1].strip()

        # Extract the level data
        tiles_begin = lines.index("[LEVEL]") + 1
        tiles_end = lines.index("[DRONES]")

        self.tiles = [list(l) for l in lines[tiles_begin:tiles_end]]

        # Center the level
        left = (GAME_WIDTH / 2) - (TILE_SIZE * len(self.tiles[0]) / 2)
        top = (GAME_HEIGHT / 2) - (TILE_SIZE * len(self.tiles) / 2)

        self.origin = (int(left), int(top))

        # Find the player start
        player_coords = (0, 0)

        for y, row in enumerate(self.tiles):
            for x, col in enumerate(row):
                if col == PLAYER_START:
                    player_coords = util.center_pos_helper(self.origin,
                                    PLAYER_SIZE, x, y)

        self.player = PlayerDrone(player_coords[0], player_coords[1])
        self.start_point = player_coords

        # Extract the enemy data
        enemy_begin = lines.index("[DRONES]") + 1
        enemy_end = lines.index("[FLAGS]")

        for line in lines[enemy_begin:enemy_end]:
            self.enemies.append(EnemyDrone(self.origin, line))

        # Extract flags and their associated objects
        flags_begin = lines.index("[FLAGS]") + 1
        flags_end = len(lines)

        # Spawn things related to flags
        profile_flags = profile.get_flags(self.world_key)
        for line in lines[flags_begin:flags_end]:
            if len(line) == 0:
                continue

            temp = line.split(':')
            flag_name = temp[0]

            tile_coords = [int(n) for n in temp[1].split(',')]
            pos = util.get_world_pos(self.origin, tile_coords[0], tile_coords[1])

            if flag_name.startswith("level"):
                active = flag_name in profile_flags
                node = Node(pos, flag_name, active)
                self.nodes.append(node)

            elif (flag_name.startswith("fragment") and
                  flag_name not in profile_flags):
                frag = Pickup(pos, DATA_FRAGMENT, flag_name)
                self.pickups.append(frag)

            elif (flag_name.startswith("key") and
                  flag_name not in profile_flags):
                key_num = int(flag_name.split('-')[1])
                key = Pickup(pos, key_num, flag_name)
                self.pickups.append(key)

        # Alter the level based on keys obtained
        for y, row in enumerate(self.tiles):
            for x, col in enumerate(row):
                key_flag = "key-" + col
                if col in LOCK_TILES and key_flag in profile_flags:
                    self.tiles[y][x] = FLOOR

    def write_to_profile(self, prof):
        prof.update_flags(self.world_key, self.flags_triggered)

    # Upon death
    def reset_level(self):
        self.player.pos[0] = self.start_point[0]
        self.player.pos[1] = self.start_point[1]
        self.player.alive = True

        for enemy in self.enemies:
            enemy.reset()

        # Any pickups that were already obtained aren't spawned again
        # so no need to check the flags
        for pickup in self.pickups:
            pickup.respawn()

        # We need to check which nodes were activated this session
        for node in self.nodes:
            if node.flag_name in self.flags_triggered:
                node.deactivate()

        # No need to check the flags for locked blocks either since it
        # only remembers the removed ones during the session
        for coords, key_tile in self.unlocked_tiles.iteritems():
            self.tiles[coords[1]][coords[0]] = key_tile

        # Clear all flags
        self.flags_triggered.clear()

    def read_event(self, event):
        if event.type == KEYUP and event.key == K_p:
            self.paused = not self.paused
            sounds.trigger_sound("escape-key")

            # The pygame.mixer.music statements should really be in sounds.py
            # but it's the last day and I'm lazy
            if self.paused:
                self.player.on_pause()
                pygame.mixer.music.pause()
            else:
                self.player.on_unpause()
                pygame.mixer.music.unpause()

        if self.paused:
            return

        if (not self.player.alive and event.type == KEYUP
            and event.key == K_SPACE):
            self.reset_level()
            sounds.trigger_sound("respawn")
            return # or else the level will exit

        self.player.read_event(event)

    def update(self):
        if self.paused:
            return

        self.update_text()

        self.player.update()

        if self.check_solid_collision(self.player.get_rect()):
            self.resolve_collision()

        for enemy in self.enemies:
            enemy.update()

        self.check_enemy_collision()
        self.check_fire_collision()

        self.check_node_activation()
        for node in self.nodes:
            node.update()

        self.check_pickups()

        if self.player.use_key:
            self.check_exit_activation()
            self.player.use_key = False

    def update_text(self):
        if self.text_timer > 0:
            self.text_timer -= 1

            if self.text_timer == 0:
                self.event_text = None

    # For walls and nodes
    def check_solid_collision(self, rect):
        # Check collision with the walls
        for y, row in enumerate(self.tiles):
            for x, col in enumerate(row):
                if col != WALL and col not in LOCK_TILES:
                    continue

                tile_pos = util.get_world_pos(self.origin, x, y)
                tile_rect = pygame.Rect(tile_pos[0], tile_pos[1],
                                        TILE_SIZE, TILE_SIZE)

                if rect.colliderect(tile_rect):
                    return True

        # Check collision with the nodes
        for node in self.nodes:
            if rect.colliderect(node.rect):
                return True

        # no collisions were found
        return False

    def resolve_collision(self):
        # ugh this again, and of course I would screw it up at one point
        last_move = self.player.last_move

        # can't collide with the wall if you never move
        if last_move[0] == 0 and last_move[1] == 0:
            return

        cur_pos = self.player.pos[:]

        x_adjusted = False
        y_adjusted = False

        # Check each axis independently first
        x_rect = pygame.Rect(cur_pos[0], cur_pos[1] - last_move[1],
                             PLAYER_SIZE, PLAYER_SIZE)
        if self.check_solid_collision(x_rect):
            x_adjusted = True

            # first truncate the float in the x coord
            # the while condition will check it again
            x_rect.x = int(x_rect.x)

            # adjust by one until there's no collision
            while self.check_solid_collision(x_rect):
                if last_move[0] > 0:
                    x_rect.x -= 1
                elif last_move[0] < 0:
                    x_rect.x += 1

            # we found a place where there's no collision on that axis
            self.player.pos[0] = x_rect.x

        y_rect = pygame.Rect(cur_pos[0] - last_move[0], cur_pos[1],
                             PLAYER_SIZE, PLAYER_SIZE)
        if self.check_solid_collision(y_rect):
            y_adjusted = True

            y_rect.y = int(y_rect.y)

            while self.check_solid_collision(y_rect):
                if last_move[1] > 0:
                    y_rect.y -= 1
                elif last_move[1] < 0:
                    y_rect.y += 1

            self.player.pos[1] = y_rect.y

        # We need to check both axes at the same time if the per-axis
        # tests did not detect collisions, as it is possible that diagonal
        # movement happened
        both_rect = self.player.get_rect()
        if (not x_adjusted and not y_adjusted and
            self.check_solid_collision(both_rect)):

            both_rect.x = int(both_rect.x)
            both_rect.y = int(both_rect.y)

            while self.check_solid_collision(both_rect):
                if last_move[0] > 0:
                    both_rect.x -= 1
                elif last_move[0] < 0:
                    both_rect.x += 1

                if last_move[1] > 0:
                    both_rect.y -= 1
                elif last_move[1] < 0:
                    both_rect.y += 1

            self.player.pos[0] = both_rect.x
            self.player.pos[1] = both_rect.y

    def check_enemy_collision(self):
        if not self.player.alive:
            return

        player_rect = self.player.get_rect()

        for enemy in self.enemies:
            enemy_rect = enemy.get_rect()
            if player_rect.colliderect(enemy_rect):
                self.player.kill()
                sounds.trigger_sound("enemy-attack")
                break

    def check_fire_collision(self):
        if not self.player.alive:
            return

        player_rect = self.player.get_rect()

        for y, row in enumerate(self.tiles):
            for x, col in enumerate(row):
                if col != FIRE:
                    continue

                tile_pos = util.get_world_pos(self.origin, x, y)
                tile_rect = pygame.Rect(tile_pos[0], tile_pos[1],
                                        TILE_SIZE, TILE_SIZE)

                if player_rect.colliderect(tile_rect):
                    self.player.kill()
                    sounds.trigger_sound("fire")
                    break

    def check_node_activation(self):
        if not self.player.alive:
            return

        player_rect = self.player.get_rect()

        for node in self.nodes:
            if player_rect.colliderect(node.detect_rect) and not node.active:
                node.activate()
                sounds.trigger_sound("node-activate")
                self.flags_triggered.add(node.flag_name)

                self.event_text = "A data node was activated, and a new device is now accessible."
                self.text_timer = 60 * 10

    def check_pickups(self):
        if not self.player.alive:
            return

        player_rect = self.player.get_rect()

        for pickup in self.pickups:
            if player_rect.colliderect(pickup.rect) and not pickup.picked_up:
                pickup.pick_up()
                sounds.trigger_sound("item-pickup")
                self.flags_triggered.add(pickup.flag_name)

                if pickup.item_type in KEYS:
                    key_num = pickup.flag_name.split('-')[1]
                    play_wall_sound = False

                    for y, row in enumerate(self.tiles):
                        for x, col in enumerate(row):
                            if col == key_num:
                                self.tiles[y][x] = FLOOR
                                # In case the player dies after
                                self.unlocked_tiles[(x, y)] = key_num
                                play_wall_sound = True

                    if play_wall_sound:
                        sounds.trigger_sound("walls-lower")

                    self.event_text = ("A key was obtained. Any barriers of the same color " +
                                       "in other devices within the data set are now removed.")
                    self.text_timer = 60 * 10

                elif pickup.item_type == DATA_FRAGMENT:
                    self.event_text = "A fragment was obtained."
                    self.text_timer = 60 * 10

    def check_exit_activation(self):
        if not self.player.alive:
            return

        if self.check_exit_tile():
            self.exiting = True
            sounds.trigger_sound("entry-port")

    def check_exit_tile(self):
        start_point_rect = pygame.Rect(self.start_point[0], self.start_point[1],
                                       PLAYER_SIZE, PLAYER_SIZE)
        player_rect = self.player.get_rect()

        return player_rect.colliderect(start_point_rect)

    def render(self, surf):
        # Render UI elements
        util.draw_frame(surf, (0, 0, 100, 40))
        font.draw_text(surf, self.font, self.name, topleft=(10, 10))

        help_rect = pygame.Rect(0, 580, 800, 20)
        help_text = self.get_help_text()
        help_pos = (2, 580)

        util.draw_frame(surf, help_rect)
        font.draw_text(surf, self.font, help_text, help_pos)

        for y, row in enumerate(self.tiles):
            for x, col in enumerate(row):
                pos = util.get_world_pos(self.origin, x, y)

                if col == WALL:
                    surf.blit(self.wall_tile, pos)
                elif col == FLOOR:
                    surf.blit(self.floor_tile, pos)
                elif col == FIRE:
                    surf.blit(self.firewall_tile, pos)
                elif col in LOCK_TILES:
                    key_rect = (TILE_SIZE * LOCK_TILES.index(col), 0,
                                TILE_SIZE, TILE_SIZE)
                    surf.blit(self.locked_tiles, pos, key_rect)
                elif col == PLAYER_START:
                    surf.blit(self.start_tile, pos)

        self.player.render(surf)

        for enemy in self.enemies:
            enemy.render(surf)

        for node in self.nodes:
            node.render(surf)

        for pickup in self.pickups:
            pickup.render(surf)

    def get_help_text(self):
        if self.paused:
            return ("The game is paused. Press P to unpause, or Escape to exit this device. " +
                    "Progress will not be saved if you exit in this way.")
        elif not self.player.alive:
            self.event_text = None
            self.text_timer = 0
            return ("The drone was destroyed. Press Spacebar to send in another one. " +
                    "Any progress made during this attempt has been lost.")
        elif self.check_exit_tile():
            self.event_text = None
            self.text_timer = 0
            return ("Use the arrow keys to move. Press Spacebar to exit the device. " +
                    "Doing so will save your progress.")
        elif self.event_text is not None:
            return self.event_text
        else:
            return "Use the arrow keys to move. Press P to pause."

# Can be a key or fragment
class Pickup:
    def __init__(self, pos, item_type, flag):
        self.rect = None
        self.draw_pos = None
        self.item_type = item_type
        self.flag_name = flag

        self.image = pygame.image.load(data.filepath("pickups.png"))
        self.picked_up = False

        if item_type == DATA_FRAGMENT:
            self.rect = pygame.Rect(pos[0], pos[1], PICKUP_SIZE, PICKUP_SIZE)
        elif item_type in KEYS:
            self.rect = pygame.Rect(pos[0] + KEY_OFFSET_X, pos[1] + KEY_OFFSET_Y,
                            KEY_HITBOX_X, KEY_HITBOX_Y)
        else:
            raise ValueError("invalid item type: " + item_type)

        self.draw_pos = (pos[0], pos[1])

    def pick_up(self):
        self.picked_up = True

    def respawn(self):
        self.picked_up = False

    def render(self, surf):
        if self.picked_up:
            return

        surf.blit(self.image, self.draw_pos,
                 (PICKUP_SIZE * self.item_type, 0, PICKUP_SIZE, PICKUP_SIZE))

class Node:
    def __init__(self, pos, flag, active=False):
        self.rect = None # collision
        self.flag_name = flag
        self.active = active

        self.detect_rect = None
        self.image = pygame.image.load(data.filepath("node.png"))
        self.node_state = IDLE
        self.frame = 0 if not self.active else 3
        self.frame_timer = 0

        self.rect = pygame.Rect(pos[0], pos[1], NODE_SIZE, NODE_SIZE)
        self.rect.x += NODE_OFFSET
        self.rect.y += NODE_OFFSET

        self.detect_rect = pygame.Rect(0, 0, NODE_DETECT, NODE_DETECT)
        self.detect_rect.center = self.rect.center

    def activate(self):
        if self.active:
            return

        self.active = True
        self.node_state = ACTIVATING
        self.frame_timer = 2

    def deactivate(self):
        if not self.active:
            return

        self.active = False
        self.node_state = DEACTIVATING
        self.frame_timer = 2

    def update(self):
        if self.node_state == IDLE:
            return

        self.frame_timer -= 1

        # Ugh this is awful code
        if self.node_state == ACTIVATING:
            if self.frame_timer == 0:
                self.frame += 1

                if self.frame != 3:
                    self.frame_timer = 2
                else:
                    self.node_state = IDLE

        elif self.node_state == DEACTIVATING:
            if self.frame_timer == 0:
                self.frame -= 1

                if self.frame != 0:
                    self.frame_timer = 2
                else:
                    self.node_state = IDLE

    def render(self, surf):
        surf.blit(self.image, self.rect.topleft,
                 (NODE_SIZE * self.frame, 0, NODE_SIZE, NODE_SIZE))
