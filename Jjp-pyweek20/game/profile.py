'''user profile; stores the player's progress'''

from __future__ import division, print_function, unicode_literals

import pickle
import os

import copy
import data

SAVE_PY = os.path.abspath(os.path.dirname(__file__))
SAVE_PATH = os.path.normpath(os.path.join(SAVE_PY, '..', "save.dat"))

# Used in the profile dict
DONE = "done"
FLAGS = "flags"
GRID = "grid"
FRAG_POS = "frag_pos"

class Profile:
    def __init__(self):
        self.world_data = {}
        self.play_music = True
        self.play_sound = True

    def init_world_data(self, world):
        if world.key in self.world_data:
            raise ValueError("data for key " + key + " already exists")

        self.world_data[world.key] = {}
        self.world_data[world.key][FLAGS] = set()
        self.world_data[world.key][GRID] = world.puzzle_grid[:]
        self.world_data[world.key][FRAG_POS] = {}  # flag -> [x, y]
        self.world_data[world.key][DONE] = False

    def get_flags(self, key):
        if key not in self.world_data:
            raise ValueError("flags for key " + key + " doesn't exist")

        return copy.deepcopy(self.world_data[key][FLAGS])

    def update_flags(self, key, flags):
        if key not in self.world_data:
            raise ValueError("flags for key " + key + " doesn't exist")

        # Flags are never removed, only added, so a set union is fine
        self.world_data[key][FLAGS] |= flags

    def get_grid(self, key):
        if key not in self.world_data:
            raise ValueError("grid for key " + key + " doesn't exist")

        # Make sure to return only a copy
        return copy.deepcopy(self.world_data[key][GRID])

    def update_grid(self, key, grid):
        if key not in self.world_data:
            raise ValueError("grid for key " + key + " doesn't exist")

        # Make sure to copy it
        self.world_data[key][GRID] = copy.deepcopy(grid)

    def get_frag_pos(self, key):
        if key not in self.world_data:
            raise ValueError("grid for key " + key + " doesn't exist")

        return copy.deepcopy(self.world_data[key][FRAG_POS])

    def update_frag_pos(self, key, frag_dict):
        if key not in self.world_data:
            raise ValueError("grid for key " + key + " doesn't exist")

        self.world_data[key][FRAG_POS] = copy.deepcopy(frag_dict)

    def is_world_done(self, key):
        if key not in self.world_data:
            # The player may not have unlocked the world, so don't raise
            # an exception for this
            return False

        return self.world_data[key][DONE]

    def mark_world_done(self, key, done=True):
        if key not in self.world_data:
            raise ValueError("bool for key " + key + " doesn't exist")

        self.world_data[key][DONE] = done

def save_profile(prof):
    try:
        fout = open(SAVE_PATH, 'wb', pickle.HIGHEST_PROTOCOL)
        pickle.dump(prof, fout)
        return True
    except IOError:
        return False

# Returns None if the file doesn't exist
def load_profile():
    try:
        if not os.path.isfile(SAVE_PATH):
            return None

        fin = open(SAVE_PATH, 'rb')
        return pickle.load(fin)
    except IOError:
        return None
