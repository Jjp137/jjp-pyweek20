Fragmented ~A Dying Land~
==========================

Entry in PyWeek #20  <https://pyweek.org/20/>
URL: https://pyweek.org/e/Jjp-pyweek20/
Team: Jjp-pyweek20
Members: Jjp137
License: see LICENSE.txt for full details

Dependencies
------------

Python 2.7 and PyGame 1.9.1 is required for this game to run.

This game is untested on Python 3.


Running the Game
----------------

On Windows or Mac OS X, locate the "run_game.pyw" file and double-click it.

Otherwise open a terminal / console and "cd" to the game directory and run:

  python run_game.py

The game will open in an 800x600 window.

There are no command line parameters.


How to Play
-----------

During levels:
- Arrow keys - Move
- Spacebar - Use portal / Respawn (when dead)
- P - Pause / Unpause
- Escape (while paused) - Exit level (does not save progress)

In menus, use the left mouse button to select items.
In the Data Repair screen, use the mouse to drag and drop the fragments onto
the grid.

Your objective is to recover data fragments from groups of devices, known as
Data Sets, and piece them together to find out more about what happened in the
past.

To do so, you have to send a tiny drone into devices and navigate it through
mazes.

Select Start Game in the main menu. On the Select Module screen, you can read
the Prologue, or ignore the story altogether and jump straight into Data Set 0.
On the Select Device screen, click Device 0-0 to begin exploring.

Use the arrow keys to move around. While you're exploring, these are the items
you may encounter:
- Nodes: These are gray circular objects that emit a blue light when you get
near them, activating them. Activated nodes unlock new devices.
- Fragments: These are green chips that hold a piece of the data you're trying
to recover.
- Keys: Unlocks locked walls of the same color within the entire Data Set.
- Defense drones: Red moving squares that destroy your drone on contact. They
always follow the same path over and over.
- Firewalls: Orange squares that burn your drone on contact. Even the slightest
touch will destroy your drone, so be careful!

Once you are done, return to the blue square that you started on and press
Spacebar to go back to the Device Select screen. This will save your progress.
You can pause by pressing P, and you can quit the current level by pressing
Escape while paused. This, however, will cause all unsaved progress to be lost.

Note that if your drone gets destroyed, you lose all progress made during that
attempt. Fortunately, progress made during previous successful attempts will
never be undone, even if a later attempt on that particular level ends in
failure.

Once you have found all the fragments, select Repair Data in the Select Device
screen, and drag and drop the fragments until all of them fit together
perfectly in the grid.

Once you have done so, the data is repaired, and you can view the analysis of
the recovered data using the View Analysis button in the Device Select screen.
This also unlocks another Data Set.

The game saves your progress when you quit.

This should be enough to get you started. Have fun! :)

Credits
-------

Jjp137 wrote the code and designed the levels.

The artwork featured in this game comes from opengameart.org.
In particular the artwork used in this game comes from the following users:
AngryMeteor.com, AntumDeluge, bart, Buch, Downdate, Kenney, Master484,
n4pgamer, rubberduck, and XCVG.

The sounds featured in this game comes from opengameart.org and freesound.org.
In particular the sounds in this game comes from the following users:
From opengameart.org: Little Robot Sound Factory
From freesound.org: Benboncan, copyc4t, fons, Julien Matthey, Sergenious,
and sheepfilms.

Without their work, this game would not have been possible.
The license for the graphics and sounds vary; see LICENSE.txt for full details.

The main font used in this game is taken from:
http://openfontlibrary.org/en/font/gidole-regular
Credits to larsenwork on GitHub for creating the font. The GitHub repository
can be found here: https://github.com/larsenwork/Gidole
The font is licensed under version 1.1 of the SIL Open Font License (OFL).

The font used for the title in the main menu is taken from:
http://openfontlibrary.org/en/font/alegreya
It is owned by Juan Pablo del Peral, and it is licensed under version 1.1 of
the SIL Open Font License (OFL).

The music for this game was created by Kevin MacLeod:

"Junkyard Tribe" Kevin MacLeod (incompetech.com)
Licensed under Creative Commons: By Attribution 3.0
http://creativecommons.org/licenses/by/3.0/
The song has been converted to .ogg from the original .mp3 format.

"Echoes of Time" Kevin MacLeod (incompetech.com)
Licensed under Creative Commons: By Attribution 3.0
http://creativecommons.org/licenses/by/3.0/
The song has been converted to .ogg from the original .mp3 format.

See LICENSE.txt for more information.


Special Thanks
--------------

First, thanks to those that were involved in organizing and running this
PyWeek, as without their efforts, I would have never made this game.

Also, special thanks to LegoBricker for providing moral support while I was
creating this game. One day, I'll stop forgetting 'self' on everything and
then I don't have to rant to you about it :p

Thank you to all my other friends and family as well.

Finally, thank you for downloading and playing my game :)

- Jjp137
