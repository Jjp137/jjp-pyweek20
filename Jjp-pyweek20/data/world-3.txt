name = Data Set 3
key = world-3
story = analysis-3.txt
[LEVELS]
Device 3-0 = level3-0.txt 1,1 unlocked-0
Device 3-1 = level3-1.txt 0,1 level3-1
Device 3-2 = level3-2.txt 0,2 level3-2
Device 3-3 = level3-3.txt 1,0 level3-3
Device 3-4 = level3-4.txt 2,0 level3-4
Device 3-5 = level3-5.txt 1,2 level3-5
Device 3-6 = level3-6.txt 1,3 level3-6
Device 3-7 = level3-7.txt 0,3 level3-7
Device 3-8 = level3-8.txt 2,3 level3-8
Device 3-9 = level3-9.txt 2,1 level3-9
Device 3-10 = level3-10.txt 3,1 level3-10
[KEYS]
key-0
key-1
key-2
key-3
key-4
key-5
key-6
key-7
key-8
key-9
[FRAGMENTS]
fragment-0 0 #.|##|#.
fragment-1 1 ###|###|###
fragment-2 2 ..#|.##|##.
fragment-3 2 ##.|#.#|###
fragment-4 4 ###|.##
fragment-5 5 #..|###
fragment-6 0 .#.|###|.#.
fragment-7 1 ##
fragment-8 2 .##|##.|#..
fragment-9 3 .#|##|##
fragment-10 4 #
fragment-11 5 #.#|###
fragment-12 0 ###|###|##.
fragment-13 1 ..#|..#|###
fragment-14 2 ##|.#|##
fragment-15 3 .#|.#|##
fragment-16 4 ###|.#.
fragment-17 5 ##|##
fragment-18 3 .##|.#.|##.
fragment-19 1 ##.|###|.#.
fragment-20 4 ..#|###|.##
[GRID]
-2 -2 -2 -1 -1 -1 -1 -1 -1 -1 -2 -2 -2 -2 -2 -1 -2 -2
-2 -2 -1 -1 -1 -1 -1 -1 -1 -1 -1 -2 -2 -2 -1 -1 -1 -2
-2 -1 -1 -2 -1 -1 -1 -1 -1 -2 -1 -1 -2 -1 -1 -1 -1 -1
-1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1
-1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1
-2 -1 -1 -2 -1 -1 -1 -1 -1 -2 -1 -1 -2 -1 -1 -1 -1 -1
-2 -2 -1 -1 -1 -1 -1 -1 -1 -1 -1 -2 -2 -2 -1 -1 -1 -2
-2 -2 -2 -1 -1 -1 -1 -1 -1 -1 -2 -2 -2 -2 -2 -1 -2 -2
[CONNECTORS]
1,1 0,1 level3-1
0,1 0,2 level3-2
1,1 1,0 level3-3
1,0 2,0 level3-4
1,1 1,2 level3-5
1,2 1,3 level3-6
1,3 0,3 level3-7
1,3 2,3 level3-8
1,1 2,1 level3-9
2,1 3,1 level3-10
