name = Device 2-2
world_key = world-2
[LEVEL]
____#################
____#...............#
#####....#..#..#....#
#...#...............#
#...##############..#
#000#6666####6666#..#
#...................#
#...................#
#...#6666####6666#..#
#.@.##############..#
#...#...^^^^^^^^^...#
#####...............#
____#...^^^^^^^^^...#
____#################
[DRONES]
8,1->10,1->10,3->8,3 3
11,3->13,3->13,1->11,1 3
14,1->16,1->16,3->14,3 3
1,6->19,6 5
1,7->19,7 5
[FLAGS]
fragment-2:2,3
level2-4:5,3
level2-6:5,10
