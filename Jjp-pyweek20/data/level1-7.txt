name = Device 1-7
world_key = world-1
[LEVEL]
###################
#^^^^^^^^^^^^^^^^^#
#^...............^#
#^...............^#
#^..^^^^^^^^^^^..^#
#^..^#########^..^#
#^..^^^^^^^^^#^..^#
#^..........^#^..^#
#^..........^#^..^#
#^^^^^^^^^^^^#^..^#
##############^..^#
#....^^^^^^^^^^..^#
#....4...........^#
#....4...........^#
#@...^^^^^^^^^^^^^#
###################
[DRONES]
8,11->8,14 1
11,14->11,11 1
14,11->17,11->17,14->14,14 2
14,4->17,4->17,1->14,1 2
4,4->4,1->1,1->1,4 2
4,6->4,9->1,9->1,6 2
14,7->17,7 1
14,8->17,8 1
7,6->9,6->9,9->7,9 1
8,4->10,4->10,1->8,1 1
[FLAGS]
fragment-7:11,7
