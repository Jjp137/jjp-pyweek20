title = Log Reader :: Data Set 3 :: Analysis
Transcript of the analysis of Data Set 3, dated June 22, 7836

Robert: "Did you finally repair the data?"

Sterk: "It was really difficult, but I managed to do it after a few days."

Robert: "Great! Even I couldn't do that. You're really skilled at this now. Hopefully this will answer some of our
lingering questions. So what kind of data did you end up repairing?"

Sterk: "Well, this is apparently a conversation 'log'. It appears to be some sort of preserved conversation. This is only
a small part of it though, since the name given to this data ends in '.0001' and I don't have any other parts."

Robert: "All that work for a small piece of the puzzle. Are you able to read it, at least?"

Sterk: "Sure. I'll read it out loud so that the camcorder can capture it."

Robert: "That's a good idea."

Sterk: "Alright. Listen."
[PAGE]
[Reading begins here]

Person A: "I object to that!"

Person B: "But if we don't take action, they'll cause even more problems for every other nation on this planet!"

Person A: "And we're going to solve these problems by killing off hundreds of people and potentially starting a chain
reaction of destruction?"

Person B: "We don't have many resources left, though. We can't just keep sending people and drones over there."

Person A: "So we're just going to resort to a plan that will kill even more people?"

Person C: "Sir!"

Person B: "Yes? What is it?"

Person C: "I just obtained some new intelligence. To get to the point, they're planning to attack us within a couple
of hours."
[PAGE]
Person B: "Well, I don't think we have a choice. We have to execute Procedure #0734."

Person A: "But what if they retaliate?"

Person B: "We'll make sure that they can't."

[Reading ends here]

Sterk: "Well, that just raised even more questions."

Robert: "What exactly was the procedure that they did? It was just referred to by a number."

Sterk: "I don't know. Hmm, let me check the date on when this conversation happened. It's May 10, 2057. That places
it right in between the photo with all the people in it and the photo with all the debris in it.

Robert: "Although we can't say for certain, it's likely that whatever they ended up doing after that conversation
occurred may have led to what we saw in the latter photo."

Sterk: "If that is the case, that's incredibly depressing."
[PAGE]
Robert: "It sounded like there's was a conflict going on, and the people who were involved in that conversation were
on the losing side. It must have been a last resort plan that they performed."

Sterk: "That is true. We don't have any more information on that plan, though."

Robert: "Indeed. There's not much data left to repair here either, so I don't think we'll know unless we find some more
abandoned buildings like this one."
