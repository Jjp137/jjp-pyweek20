title = Log Reader :: Data Set 1 :: Analysis
Transcript of the analysis of Data Set 1, dated June 18, 7836

Robert: "So, what do we have here?"

Sterk: "I found some really interesting, um, what do you call these?"

Robert: "They're called 'photos,' Sterk. The best way to describe them is that they are paintings of places in the past
made with some of these devices."

Sterk: "Huh. I didn't know these devices can make such beautiful...photos, I guess."

Robert: "Woah, what is this? There's a lot going on in this photo!"

Sterk: "Definitely. Oh hey, these look like the building that we're in, only much, much taller. So tall that I feel they're
piercing the clouds. And look at all those glass windows. They're so uniformly placed. This is impressive."

Robert: "There's a lot of people on the ground too, as well as these...I think I saw something like this a while back.
That weirdly-shaped metal box on four wheels is known as a 'car', and they're much faster than horses. There's a lot
of them here."
[PAGE]
Sterk: "I don't see how they are faster than horses when there are so many of them crammed on one road."

Robert: "They definitely are. So much faster, in fact, that the term 'horsepower' describes the power of a car. It's
just that there seems to be a lot more people in one place back then."

Sterk: "Well, I'll take your word for it. Are the people on the ground using those devices that we have been finding so
far? I feel like they're not paying attention to where they are going when they use them.

Robert: "They probably bump into people a lot. I can't help but think that it's a bit rude to not pay attention to your
surroundings in a place like this."

Sterk: "I agree. There's also a flag on a pole over in the distance."

Robert: "Good eye, Sterk. I don't recognize that flag."

Sterk: "It's definitely not related to our capital at all."

Robert: "This has been interesting to look at so far, but it's been a long day, and I'm tired."
[PAGE]
Sterk: "Me too. We can continue analyzing this photo and look at the other ones tomorrow."

Robert: "We can do that. Let me ask you a question before you go."

Sterk: "Sure, what is it?"

Robert: "Why did you take this job?"

Sterk: "To be honest, I was hoping to find out what happened in the past. In particular, I want to know if something
in the past is causing this land to slowly wither away, and if so, I want to know how to stop its decline."

Robert: "A noble cause."

Sterk: "I suppose. And what's your reason?"

Robert: "Simple. I find it fun. I love learning anything about the past."

Sterk: "That's all?"
[PAGE]
Robert: "Yes. Although if we find the answers that you seek, that would be a bonus too."

Sterk: "Well, I'm glad that you're here."

Robert: "Don't mention it."
